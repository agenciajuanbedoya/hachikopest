import React, {Component} from 'react';
import {Link} from "react-router-dom";

import API from "../../core/Api";
import CardProduct from "../Cards/CardProduct";

import './styles/index.css';
import {connect} from "react-redux";
import Cookies from "js-cookie";

class GridProducts extends Component{
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      products: [],
      pageCount: 2,
      itemCount: 0,
      pages: [],
      position: 1,
    }
  }

  fetchDataSimple = async () => {
    if (this.state.position < this.state.pageCount) {
      this.setState({
        ...this.state,
        loading: true,
      });
      const res = await fetch(API + `/products/${Cookies.get('hachikopets_loc').toLowerCase()}?page=${this.state.position}&limit=16`);
      const data = await res.json();
      this.setState({
        ...this.state,
        products: data.products,
        pageCount: data.pageCount,
        itemCount: data.itemCount,
        pages: data.pages,
        position: this.state.position+1,
        loading: false,
      });
    }
  };

  fetchDataComplex = async () => {
    if (this.state.position < this.state.pageCount) {
      this.setState({
        ...this.state,
        loading: true,
      });
      const res = await fetch(API + `/${Cookies.get('hachikopets_loc').toLowerCase()}/products/pet/${this.props.pet.toLowerCase()}?page=${this.state.position}&limit=16`);
      const data = await res.json();
      this.setState({
        ...this.state,
        products: data.products,
        pageCount: data.pageCount,
        itemCount: data.itemCount,
        pages: data.pages,
        position: this.state.position+1,
        loading: false,
      });
    }
  };

  render() {
    if (this.state.loading) {
      return (
        <div className={'loading'}>
          <div className="loader">Loading...</div>
        </div>
      );
    } else if (this.props.pet === undefined) {
      return (
        <section className={'container'}>
          <div className={'row'}>
            {this.state.products.map(item => (<CardProduct data={item} key={item.id} />))}
          </div>
          <div className={'row'}>
            <div className="col-12 GridProducts__next">
              <Link
                to={'#'}
                onClick={
                  this.fetchDataSimple
                }
              >
                Cargar más artículos
              </Link>
            </div>
          </div>
        </section>
      );
    } else {
      return (
        <section className={'container'}>
          <div className={'row'}>
            {this.state.products.map(products => (<CardProduct data={products} />))}
          </div>
          <div className={'row'}>
            <div className="col-12 GridProducts__next">
              <Link
                to={'#'}
                onClick={
                  this.fetchDataComplex
                }
              >
                Cargar más artículos
              </Link>
            </div>
          </div>
        </section>
      );
    }
  }

  componentDidMount() {
    if (this.props.pet === undefined) {
      this.fetchDataSimple().then(r => {
        this.setState({ loading: false });
      });
    } else {
      this.fetchDataComplex().then(r => {
        this.setState({ loading: false });
      });
    }
  }
}

const mapStateToProps = state => ({
  location: state.location,
  pet: state.pet
});

export default connect(mapStateToProps)(GridProducts);
