import React, {Component} from 'react';
import {connect} from "react-redux";
import Cookies from "js-cookie";
import {Link} from "react-router-dom";
import API from "../../core/Api";
import CardProduct from "../Cards/CardProduct";
import './styles/index.css';

class ProductsGrids extends Component{
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      docs: [],
      total: 0,
      limit: 0,
      page: 1,
      pages: 0,
    }
  }

  fetchProducts = async () => {
    if (this.state.pages > this.state.page) {
      this.setState({
        ...this.state,
        loading: true,
      });
      const res = await fetch(API + `/products?location=${Cookies.get('hachikopets_loc').toLowerCase()}&pet=${this.props.pet}&page=${this.state.page}`);
      const data = await res.json();
      this.setState({
        ...this.state,
        docs: data.docs,
        total: data.total,
        limit: data.limit,
        page: data.page + 1,
        pages: data.pages,
        loading: false,
      });
    } else {
      this.setState({
        ...this.state,
        loading: true,
      });
      const res = await fetch(API + `/products?location=${Cookies.get('hachikopets_loc').toLowerCase()}&pet=${this.props.pet}&page=${this.state.page}`);
      const data = await res.json();
      this.setState({
        ...this.state,
        docs: data.docs,
        total: data.total,
        limit: data.limit,
        page: data.page + 1,
        pages: data.pages,
        loading: false,
      });
    }
  };

  render() {
    if (this.state.loading) {
      return (
        <div className={'loading'}>
          <div className="loader">Loading...</div>
        </div>
      );
    } else {
      return (
        <section className={'container'}>
          <div className={'row'}>
            {this.state.docs.map((products, index) => (<CardProduct data={products} key={index} />))}
          </div>
          <div className={'row'}>
            <div className="col-12 GridProducts__next">
              <Link to={'#'} onClick={this.fetchProducts}>
                Cargar más artículos
              </Link>
            </div>
          </div>
        </section>
      );
    }
  }

  componentDidMount() {
    this.fetchProducts().then(r => {
      this.setState({
        ...this.state,
        loading: false
      });
    });
  }
}

const mapStateToProps = state => ({
  location: state.location,
  pet: state.pet
});

export default connect(mapStateToProps)(ProductsGrids);
