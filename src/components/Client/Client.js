import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {ToastsStore, ToastsContainer, ToastsContainerPosition} from 'react-toasts';
import SimpleReactValidator from 'simple-react-validator';
import {database, auth} from "../../core/firebase";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCat, faDog} from "@fortawesome/free-solid-svg-icons";
import DELIVERYCheckout from "../Checkout/DELIVERYCheckout";
import './styles/Client.css';

class Client extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        doc_type: 'CC',
      },
      password: '',
      password2: '',
      terms: false,
    };
    this.validator = new SimpleReactValidator({
      locale: 'es',
      element: message => <div style={{color: 'red'}}>{message}</div>,
      messages: {
        default: 'Información requerida',
      }, autoForceUpdate: this});
  }

  handleCreateUser = e => {
    e.preventDefault();
    if (this.validator.allValid()) {
      if (this.state.terms) {
        if (this.state.password === this.state.password2) {
          auth.createUserWithEmailAndPassword(this.state.form.email, this.state.password)
            .then(r => {
              database.ref('users/' + r.user.uid)
                .set(this.state.form)
                .then(r => {
                  ToastsStore.success("Bienvenid@ " + this.state.form.name);
                })
                .then(() => {
                  this.props.handleUser(r.user);
                })
            }).catch(err => {
            ToastsStore.error(err.message);
          });
        } else {
          ToastsStore.error("Contraseña no coincide");
        }
      } else {
        ToastsStore.error("Debe aceptar términos y codiciones");
      }
    } else {
      this.validator.showMessages();
      ToastsStore.error("Error al registrarse");
      this.forceUpdate();
    }
  };

  handleChangeForm = e => {
    this.setState({
      ...this.state,
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });
  };

  handleChange = e => {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value,
    });
  };

  handleChangePets = e => {
    this.setState({
      ...this.state,
      form: {
        ...this.state.form,
        pet: e,
      },
    });
  };

  handleTerms = () => {
    if (this.state.terms === false) {
      this.setState({
        ...this.state,
        terms: true,
      });
    } else {
      this.setState({
        ...this.state,
        terms: false,
      });
    }
  };

  render() {
    if (this.props.uid === undefined) {
      return (
        <form className="needs-validation" noValidate onSubmit={this.handleCreateUser}>
          <h4 style={{ marginBottom: '30px', fontWeight: 'bold'}}>Registro</h4>
          <div className="row">
            <div className="col-md-6 mb-3">
              <label>Nombres</label>
              <input
                type="text"
                className="form-control"
                name="name"
                value={this.state.form.name}
                onChange={this.handleChangeForm}
                required
              />
              {this.validator.message('name', this.state.form.name, 'required')}
            </div>

            <div className="col-md-6 mb-3">
              <label>Apellidos</label>
              <input
                type="text"
                className="form-control"
                name="last_name"
                value={this.state.form.last_name}
                onChange={this.handleChangeForm}
                required
              />
              {this.validator.message('last_name', this.state.form.last_name, 'required')}
            </div>

            <div className="col-md-6 mb-3">
              <label>Tipo de documento</label>
              <select
                className="custom-select d-block w-100"
                name="doc_type"
                onChange={this.handleChange}
                value={this.state.form.doc_type}
              >
                <option value={'CC'}>Cédula de ciudadanía</option>
                <option value={'CE'}>Cédula de extrangería</option>
                <option value={'DNI'}>Identificación extranjera</option>
                <option value={'PPN'}>Pasaporte</option>
                <option value={'NIT'}>NIT</option>
              </select>
            </div>


            <div className="col-md-6 mb-3">
              <label>Cédula</label>
              <input
                type="number"
                min={0}
                className="form-control"
                name="doc_number"
                value={this.state.form.doc_number}
                onChange={this.handleChangeForm}
                required
              />
              {this.validator.message('doc_number', this.state.form.doc_number, 'required')}
            </div>

            <div className="col-md-6 mb-3">
              <label>Correo electrónico</label>
              <input
                type="email"
                className="form-control"
                name="email"
                value={this.state.form.email}
                onChange={this.handleChangeForm}
                required
              />
              {this.validator.message('email', this.state.form.email, 'required')}
            </div>

            <div className="col-md-6 mb-3">
              <label>Nro. de teléfono</label>
              <input
                type="phone"
                className="form-control"
                name="phone"
                value={this.state.form.phone}
                onChange={this.handleChangeForm}
                required
              />
              {this.validator.message('phone', this.state.form.phone, 'required')}
            </div>

            <div className="col-md-6 mb-3">
              <label>Contraseña</label>
              <input
                type="password"
                className="form-control"
                name="password"
                value={this.state.password}
                onChange={this.handleChange}
                required
              />
              {this.validator.message('password', this.state.password, 'required')}
            </div>
            <div className="col-md-6 mb-3">
              <label>Confirmar contraseña</label>
              <input
                type="password"
                className="form-control"
                name="password2"
                value={this.state.password2}
                onChange={this.handleChange}
                required
              />
              {this.validator.message('password2', this.state.password2, 'required')}
            </div>

            <div className="col-md-12 mb-6">
              <h6>¿Qué peludo es el dueño de tu corazón?</h6>
            </div>

            <div className="col-md-6 mb-3">
              {this.state.form.pet === 'Perro' ? (
                <div className={'ClientPet__btn-active'}>
                  <FontAwesomeIcon className={'ClientPet__btn-icon'} icon={faDog}/>
                  <span>Perro</span>
                </div>
              ) : (
                <div className={'ClientPet__btn'} component={Link} onClick={() => this.handleChangePets('Perro')}>
                  <FontAwesomeIcon className={'ClientPet__btn-icon'} icon={faDog}/>
                  <span>Perro</span>
                </div>
              )}
            </div>
            <div className="col-md-6 mb-3">
              {this.state.form.pet === 'Gato' ? (
                <div className={'ClientPet__btn-active'}>
                  <FontAwesomeIcon className={'ClientPet__btn-icon'} icon={faCat}/>
                  <span>Gato</span>
                </div>
              ) : (
                <div className={'ClientPet__btn'} component={Link} onClick={() => this.handleChangePets('Gato')}>
                  <FontAwesomeIcon className={'ClientPet__btn-icon'} icon={faCat}/>
                  <span>Gato</span>
                </div>
              )}
            </div>

            <div className="form-group form-check" style={{marginLeft: 16}}>
              <input
                type="checkbox"
                className="form-check-input"
                name={'terms'}
                checked={this.state.terms}
                onChange={this.handleTerms}
                required
              />
              <label className="form-check-label">
                Acepto los términos y condiciones de uso (<Link to={'/legal'}>Ver</Link>)
              </label>
            </div>
          </div>

          <ToastsContainer position={ToastsContainerPosition.BOTTOM_CENTER} store={ToastsStore}/>
          <div className="row">
            <div className="col-12">
              <input
                className={"btn btn-primary btn-lg btn-block"}
                value={'Registrarme'}
                type={"submit"}
              />
            </div>
          </div>
          <ToastsContainer position={ToastsContainerPosition.BOTTOM_CENTER} store={ToastsStore}/>
        </form>
      );
    } else {
      return (
        <DELIVERYCheckout />
      );
    }
  }
}

const mapStateToProps = state => ({
  uid: state.user.uid,
});

const mapDispatchToProps = dispatch => ({
  handleUser(user){
    dispatch({
      type: 'SET_USER',
      user
    })
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Client);
