import React, {Component, useState} from "react";
import {Modal} from "react-bootstrap";
import {connect} from "react-redux";
import Cookies from 'js-cookie';
import {Link} from "react-router-dom";
import imgPerro from "../../images/selector/perro.png";
import imgGatos from "../../images/selector/gatos.png";
import CardSelectorPet from "../Cards/CardSelectorPet";

const Selector = () => {
  const [modalShow, setModalShow] = useState(true);
  return (
    <Modal
      show={modalShow}
      onHide={() => setModalShow(false)}
      centered
      backdrop={'static'}
    >
      <Modal.Body>
        <div className={'container'}>
          <div className={'row'}>

            <Link to={'#'} onClick={() => {
              Cookies.set('hachikopets_pet', 'Perros', { expires: 7, path: '/' });
              setModalShow(false);
            }} className={'col-xl-6 col-lg-6 col-md-6 col-sm-12'}>
              <CardSelectorPet title={'Perros'} image={imgPerro} />
            </Link>

            <Link to={'#'} onClick={() => {
              Cookies.set('hachikopets_pet', 'Gatos', { expires: 7, path: '/' });
              setModalShow(false);
            }} className={'col-xl-6 col-lg-6 col-md-6 col-sm-12'}>
              <CardSelectorPet title={'Gatos'} image={imgGatos} />
            </Link>

          </div>
        </div>
      </Modal.Body>
    </Modal>
  );
};

class PetModal extends Component {
  render() {
    if (Cookies.get('hachikopets_pet') === undefined) {
      return <Selector/>;
    } else {
      this.props.handlePet(Cookies.get('hachikopets_pet'));
      return (
        <div>
          {console.log(Cookies.get('hachikopets_pet'))}
        </div>
      );
    }
  }
}

const mapStateToProps = state => ({
  pet: state.pet,
  location: state.location
});

const mapDispatchToProps = dispatch => ({
  handlePet(pet){
    dispatch({
      type: 'CHANGE_PET',
      pet
    })
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(PetModal);
