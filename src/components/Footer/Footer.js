import React, {Component} from "react";
import {faFacebookF, faInstagram, faWhatsapp} from "@fortawesome/free-brands-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Logo from '../../images/icons/Logo-Hachiko-pets3bLANCO.png';
import './Styles/Footer.css';

class Footer extends Component {
  render() {
    return (
      <div>
        <div className={'Footer__border-container'} style={{backgroundColor: this.props.color}}/>
        <div className={'Footer__container-dark'}>
          <div className={'container'}>
            <div className={'row align-items-center justify-content-center'}>
              <div className={'col-xl-6 col-lg-6 col-md-6'}>
                <img src={Logo} width={'100%'} alt={'Logo Hachiko Pets'}/>
              </div>
              <div className={'col-xl-6 col-lg-6 col-md-6'}>
                <div className={'row align-items-center justify-content-center'}>
                  <div className={'Footer__container-dark-icon col-auto'}>
                    <a href="https://www.facebook.com/hachikopetslovers/" rel="noopener noreferrer">
                      <FontAwesomeIcon icon={faFacebookF}/>
                    </a>
                  </div>
                  <div className={'Footer__container-dark-icon col-auto'}>
                    <a href="https://www.instagram.com/hachiko_pets/" rel="noopener noreferrer">
                      <FontAwesomeIcon icon={faInstagram}/>
                    </a>
                  </div>
                  <div className={'Footer__container-dark-icon col-auto'}>
                    <a href="https://wa.me/+573194775967" rel="noopener noreferrer">
                      <FontAwesomeIcon icon={faWhatsapp}/>
                    </a>
                  </div>
                  <div className={'col-12 text-center'}>
                    <p className={'Footer__container-dark-text'}>
                      hachikopets.com <br />
                      Contacto <br />
                      Dirección: Dg 182 No 20 – 17 Bogotá <br />
                      Teléfono: (319) 477-5967
                    </p>
                  </div>
                </div>
              </div>
              <div className={'Footer__container-dark-copyright col-auto align-items-center justify-content-center'}>
                <span className={'Footer__container-dark-text-copyright'}>Hachiko Pets © 2020. Desarrollado por <a
                  href="https://www.agenciajuanbedoya.com/">Agencia Juan Bedoya</a></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
