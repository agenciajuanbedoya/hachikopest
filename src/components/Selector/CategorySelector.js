import React, {Component} from "react";
import {connect} from "react-redux";
import {CardCategory} from "../Cards/CardCategory";
import bgImage from "../../images/bg-category.png";
import icImage1 from "../../images/perro1.png";
import icImage2 from "../../images/perro2.png";
import icImage3 from "../../images/perro3.png";
import icImage4 from "../../images/perro4.png";
import './style/index.css';

class CategorySelector extends Component {
  render() {
    if (this.props.pet === 'Perros') {
      return (
        <section className={'container CategorySelector__root animated fadeInUp fast'}>
          <div className={'row CategorySelector__container'} style={{ backgroundImage: `url(${bgImage})` }}>
            <div className={'col-xl-8 col-lg-8 col-md-12'}>
              <h1 className={'CategorySelector__title text-center'}>Conciente a tu perro</h1>
              <div className={'row'}>
                <CardCategory title={'Accesorios'} image={icImage1} action={this.props.action} link={'/category/Accesorios'} />
                <CardCategory title={'Alimentos'} image={icImage2} action={this.props.action} link={'/category/Alimentos'} />
                <CardCategory title={'Medicinas'} image={icImage3} action={this.props.action} link={'/category/Medicinas'} />
                <CardCategory title={'Otros'} image={icImage4} action={this.props.action} link={'/category/Otros'} />
              </div>
            </div>
          </div>
        </section>
      );
    } else {
      return (
        <section className={'container CategorySelector__root animated fadeInUp fast'}>
          <div className={'row CategorySelector__container'} style={{ backgroundImage: `url(${bgImage})` }}>
            <div className={'col-xl-8 col-lg-8 col-md-12'}>
              <h1 className={'CategorySelector__title text-center'}>Conciente a tu gato</h1>
              <div className={'row'}>
                <CardCategory title={'Arenas'} image={icImage4} action={this.props.action} link={'/category/Arenas'} />
                <CardCategory title={'Accesorios'} image={icImage1} action={this.props.action} link={'/category/Accesorios'} />
                <CardCategory title={'Alimentos'} image={icImage2} action={this.props.action} link={'/category/Alimentos'} />
                <CardCategory title={'Medicinas'} image={icImage3} action={this.props.action} link={'/category/Medicinas'} />
              </div>
            </div>
          </div>
        </section>
      );
    }
  }
}

const mapStateToProps = state => ({
  location: state.location,
  pet: state.pet
});

export default connect(mapStateToProps)(CategorySelector);
