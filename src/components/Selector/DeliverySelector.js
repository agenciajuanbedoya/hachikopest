import React, {Component, Fragment} from "react";
import DatePicker, {registerLocale} from "react-datepicker";
import {addDays, getDay} from 'date-fns';
import {connect} from "react-redux";
import {Link} from 'react-router-dom'
import {es} from 'date-fns/locale';
import {TimeApi} from "../../core/Api";
import Checkout from "../Checkout";
import './style/DeliverySelector.css';

registerLocale('es', es);

const Regular = (props) => {
  const formatHour = date => {
    const nowTime = new Date(date);
    return nowTime.getHours()
  };

  const formatPeso = new Intl.NumberFormat('es-CO', {
    style: 'currency',
    currency: 'COP',
    minimumFractionDigits: 0
  });

  if (formatHour(props.nowTime) >= 8 && formatHour(props.nowTime) <= 23 && props.title === 'Mañana') {
    return (
      <div className={'DeliverySelector__item DeliverySelector__item-disable'}>
        <span><b>{props.title}</b>: {formatPeso.format(props.info)}</span>
      </div>
    );
  }

  if (formatHour(props.nowTime) >= 12 && formatHour(props.nowTime) <= 23 && props.title === 'Tarde') {
    return (
      <div className={'DeliverySelector__item DeliverySelector__item-disable'}>
        <span><b>{props.title}</b>: {formatPeso.format(props.info)}</span>
      </div>
    );
  }

  if (formatHour(props.nowTime) >= 16 && formatHour(props.nowTime) <= 23 && props.title === 'Noche') {
    return (
      <div className={'DeliverySelector__item DeliverySelector__item-disable'}>
        <span><b>{props.title}</b>: {formatPeso.format(props.info)}</span>
      </div>
    );
  }

  if (props.timezone === props.title) {
    return (
      <div className={'DeliverySelector__item DeliverySelector__item-select'}>
        <span><b>{props.title}</b>: {formatPeso.format(props.info)}</span>
      </div>
    );
  }

  return (
    <div className={'DeliverySelector__item DeliverySelector__item-regular'}>
      <span><b>{props.title}</b>: {formatPeso.format(props.info)}</span>
    </div>
  );

};

const Express = (props) => {
  const formatHour = date => {
    const nowTime = new Date(date);
    return nowTime.getHours()
  };

  const formatPeso = new Intl.NumberFormat('es-CO', {
    style: 'currency',
    currency: 'COP',
    minimumFractionDigits: 0
  });

  if (formatHour(props.nowTime) >=16 && formatHour(props.nowTime) <= 23) {
    return(
      <div className={'DeliverySelector__item DeliverySelector__item-disable'}>
        <span><b>{props.title}</b>: {formatPeso.format(props.info)}</span>
      </div>
    );
  }

  if (props.timezone === props.title) {
    return (
      <div className={'DeliverySelector__item DeliverySelector__item-select'}>
        <span><b>{props.title}</b>: {formatPeso.format(props.info)}</span>
      </div>
    );
  }

  return(
    <div className={'DeliverySelector__item DeliverySelector__item-express'}>
      <span><b>{props.title}</b>: {formatPeso.format(props.info)}</span>
    </div>
  );
};

const RegularAnotherDay = (props) => {
  const formatPeso = new Intl.NumberFormat('es-CO', {
    style: 'currency',
    currency: 'COP',
    minimumFractionDigits: 0
  });

  if (props.timezone === props.title) {
    return (
      <div className={'DeliverySelector__item DeliverySelector__item-select'}>
        <span><b>{props.title}</b>: {formatPeso.format(props.info)}</span>
      </div>
    );
  }

  return (
    <div className={'DeliverySelector__item DeliverySelector__item-regular'}>
      <span><b>{props.title}</b>: {formatPeso.format(props.info)}</span>
    </div>
  );
};

const ExpressAnotherDay = (props) => {
  const formatPeso = new Intl.NumberFormat('es-CO', {
    style: 'currency',
    currency: 'COP',
    minimumFractionDigits: 0
  });

  if (props.timezone === props.title) {
    return (
      <div className={'DeliverySelector__item DeliverySelector__item-select'}>
        <span><b>{props.title}</b>: {formatPeso.format(props.info)}</span>
      </div>
    );
  }

  return(
    <div className={'DeliverySelector__item DeliverySelector__item-express'}>
      <span><b>{props.title}</b>: {formatPeso.format(props.info)}</span>
    </div>
  );
};

class DeliverySelector extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todayDate: new Date(),
      selectDate: new Date(),
      todayHour: {
        datetime: '',
      },
    };
  }

  fetchTimeApi = async () => {
    const res = await fetch(TimeApi);
    const data = await res.json();
    this.setState({
      ...this.state,
      todayHour: data,
    })
  };

  handleChange = date => {
    this.setState({
      ...this.state,
      selectDate: date
    });
    this.props.handleDate(date);
  };

  isWeekday = date => {
  const day = getDay(date);
  return day !== 0;
  };

  render() {
    const formatDate = date => {
      const nowTime = new Date(date);
      return `${nowTime.getDate()}/${nowTime.getMonth() + 1}/${nowTime.getFullYear()}`;
    };

    if (this.props.timezoneVerify === 0) {
      if (formatDate(this.state.todayHour.datetime) === formatDate(this.state.selectDate)) {
        return (
          <Fragment>
            <h4 className="mb-3">Horarios de entrega</h4>

            <DatePicker
              selected={this.state.selectDate}
              onChange={this.handleChange}
              minDate={this.state.todayDate}
              maxDate={addDays(new Date(), 14)}
              dateFormat="dd/MM/yyyy"
              filterDate={this.isWeekday}
              fixedHeight
              locale="es"
              inline
            />

            <Link to={'#'} onClick={() => this.props.handleTimezone({title: 'Mañana', price: 3500})}>
              <Regular
                title={'Mañana'}
                info={'3500'}
                nowTime={this.state.todayHour.datetime}
                dateSelect={this.state.selectDate}
                timezone={this.props.timezone}
              />
            </Link>

            <Link to={'#'} onClick={() => this.props.handleTimezone({title: 'Tarde', price: 3500})}>
              <Regular
                title={'Tarde'}
                info={'3500'}
                nowTime={this.state.todayHour.datetime}
                dateSelect={this.state.selectDate}
                timezone={this.props.timezone}
              />
            </Link>

            <Link to={'#'} onClick={() => this.props.handleTimezone({title: 'Noche', price: 3500})}>
              <Regular
                title={'Noche'}
                info={'3500'}
                nowTime={this.state.todayHour.datetime}
                dateSelect={this.state.selectDate}
                timezone={this.props.timezone}
              />
            </Link>

            <Link to={'#'} onClick={() => this.props.handleTimezone({title: 'Express', price: 11000})}>
              <Express
                title={'Express'}
                info={'11000'}
                nowTime={this.state.todayHour.datetime}
                dateSelect={this.state.selectDate}
                timezone={this.props.timezone}
              />
            </Link>
            <div component={Link} className={'DeliverySelector__back'} onClick={() => this.props.handleResetDelivery(undefined)}>
              Regresar
            </div>
          </Fragment>
        );
      } else {
        return (
          <Fragment>
            <h4 className="mb-3">Horarios de entrega</h4>

            <DatePicker
              selected={this.state.selectDate}
              onChange={this.handleChange}
              minDate={this.state.todayDate}
              maxDate={addDays(new Date(), 14)}
              dateFormat="dd/MM/yyyy"
              locale="es"
              inline
              fixedHeight
              filterDate={this.isWeekday}
            />

            <Link to={'#'} onClick={() => this.props.handleTimezone({title: 'Mañana', price: 3500})}>
              <RegularAnotherDay
                title={'Mañana'}
                info={'3500'}
                timezone={this.props.timezone}
              />
            </Link>

            <Link to={'#'} onClick={() => this.props.handleTimezone({title: 'Tarde', price: 3500})}>
              <RegularAnotherDay
                title={'Tarde'}
                info={'3500'}
                timezone={this.props.timezone}
              />
            </Link>

            <Link to={'#'} onClick={() => this.props.handleTimezone({title: 'Noche', price: 3500})}>
              <RegularAnotherDay
                title={'Noche'}
                info={'3500'}
                timezone={this.props.timezone}
              />
            </Link>

            <Link to={'#'} onClick={() => this.props.handleTimezone({title: 'Express', price: 11000})}>
              <ExpressAnotherDay
                title={'Express'}
                info={'11000'}
                timezone={this.props.timezone}
              />
            </Link>

            <div component={Link} className={'DeliverySelector__back'} onClick={() => this.props.handleResetDelivery(undefined)}>
              Regresar
            </div>
          </Fragment>
        );
      }
    } else {
      return <Checkout />
    }
  }

  componentDidMount() {
    this.fetchTimeApi()
  }

  componentWillUnmount() {
    this.fetchTimeApi()
  }
}

const mapStateToProps = state => ({
  location: state.location,
  timezone: state.timezone.title,
  timezoneVerify: state.timezone.price,
  date: state.date,
  pet: state.pet,
});

const mapDispatchToProps = dispatch => ({
  handleResetDelivery(status){
    dispatch({
      type: 'RESET_DELIVERY',
      status
    })
  },
  handleDate(date){
    dispatch({
      type: 'SET_DATE_SELECT',
      date
    })
  },
  handleTimezone(timezone){
    dispatch({
      type: 'SET_TIME_ZONE',
      timezone
    })
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(DeliverySelector);
