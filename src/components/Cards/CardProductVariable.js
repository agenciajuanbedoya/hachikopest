import React, {Component} from "react";
import { Link } from "react-router-dom";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFire, faShoppingCart} from "@fortawesome/free-solid-svg-icons";

import './styles/Product.css';

class CardProductVariable extends Component {
  render() {
    return (
      <div className={'col-12 Product__root'}>
        <div className={'row justify-content-center'}>
          <div className={'col-11 Product__header'}>
            <img
              className={'Product__image'}
              src={this.props.data.images}
              alt={this.props.data.title} />
            <div className={'Product__header-content row justify-content-between'}>
              <div className={'col-auto'}>
                <FontAwesomeIcon icon={faFire} /> -10%
              </div>
            </div>
            <div className={'Product__options'}>
              <div className={'Product__options-container'}>
                <h2 className={'Product__options-title'}>{this.props.data.title}</h2>
                <div className={'Product__options-btn dropdown'}>
                  <button
                    className="Product__options-btn-primary dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false">
                  </button>
                  <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a className="dropdown-item" href="/">1 kg</a>
                    <a className="dropdown-item" href="/">25 kg</a>
                    <a className="dropdown-item" href="/">50 kg</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className={'col-11 Product__content'}>
            <Link to={"/product/"+this.props.data.id}>
              <div className={'row justify-content-between align-items-center'}>
                <div className={'col-auto'}>
                  <div className={'Product__content-title'}>
                    Desde:
                  </div>
                  <div className={'Product__content-price'}>
                    $ {this.props.data.price}
                  </div>
                </div>
                <div className={'col-auto'}>
                  <div className={'Product__content-btn'}>
                    <FontAwesomeIcon className={'Header-Top__icon'} icon={faShoppingCart} />
                  </div>
                </div>
              </div>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default CardProductVariable;
