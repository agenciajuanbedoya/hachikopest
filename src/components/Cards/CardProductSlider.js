import React, {Component} from "react";
import { Link } from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFire, faShoppingCart} from "@fortawesome/free-solid-svg-icons";
import {connect} from "react-redux";
import Cookies from "js-cookie";
import API from "../../core/Api";
import './styles/Product.css';

class CardProductSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      variations: [],
      product: this.props.data,
      offer: this.props.data.discount,
    };
  }
  fetchData = () => {
    fetch(API + `/product/var?location=${Cookies.get('hachikopets_loc').toLowerCase()}&sku=${this.state.product.sku}&page=1`)
      .then(res => res.json())
      .then(res => {
        this.setState({
          ...this.state,
          variation: res.docs,
          product: {
            ...this.state.product,
            price: parseInt(res.docs[0].price),
          },
        });
      });
  };
  formatPeso = new Intl.NumberFormat('es-CO', {
    style: 'currency',
    currency: 'COP',
    minimumFractionDigits: 0
  });
  render() {
    let offer;

    if (this.state.offer !== 0) {
      offer = (
        <div className={'Product__header-content row justify-content-between'}>
          <div className={'col-auto'}>
            <FontAwesomeIcon icon={faFire} /> -{this.state.offer}%
          </div>
        </div>
      );
    }
    return (
      <div className={'col-12 Product__root'}>
        <div className={'row justify-content-center'} key={this.state.product.key}>
          <div className={'col-11 Product__header'}>
            <img
              className={'Product__image'}
              src={this.state.product.images}
              alt={this.state.product.title} />
            {offer}
            <div className={'Product__options'}>
              <div className={'Product__options-container'}>
                <h2 className={'Product__options-title'}>{this.state.product.title}</h2>
                {/*<div className={'Product__options-btn dropdown'}>*/}
                {/*  <button*/}
                {/*    className="Product__options-btn-primary dropdown-toggle"*/}
                {/*    type="button"*/}
                {/*    id="dropdownMenuButton"*/}
                {/*    data-toggle="dropdown"*/}
                {/*    aria-haspopup="true"*/}
                {/*    aria-expanded="false">*/}
                {/*  </button>*/}
                {/*  <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">*/}
                {/*    <a className="dropdown-item" href="/">1 kg</a>*/}
                {/*    <a className="dropdown-item" href="/">25 kg</a>*/}
                {/*    <a className="dropdown-item" href="/">50 kg</a>*/}
                {/*  </div>*/}
                {/*</div>*/}
              </div>
            </div>
          </div>
          <div className={'col-11 Product__content'}>
            <Link to={"/product/"+this.state.product._id}>
              <div className={'row justify-content-between align-items-center'}>
                <div className={'col-auto'}>
                  <div className={'Product__content-title'}>
                    Desde:
                  </div>
                  <div className={'Product__content-price'}>
                    {this.formatPeso.format(this.state.product.price)}
                  </div>
                </div>
                <div className={'col-auto'}>
                  <div className={'Product__content-btn'}>
                    <FontAwesomeIcon className={'Header-Top__icon'} icon={faShoppingCart} />
                  </div>
                </div>
              </div>
            </Link>
          </div>
        </div>
      </div>
    );
  }
  componentDidMount() {
    if (this.state.product.type !== "simple") {
      this.fetchData();
    }
    console.clear();
  }
  componentWillUnmount() {
    if (this.state.product.type !== "simple") {
      this.fetchData();
    }
  }
}

const mapStateToProps = state => ({
  pet: state.pet,
  location: state.location
});

export default connect(mapStateToProps)(CardProductSlider);
