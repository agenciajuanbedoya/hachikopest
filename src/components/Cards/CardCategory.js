import React from "react";
import {Link} from "react-router-dom";
import './styles/CardCategory.css';

export const CardCategory = (props) => {
  return(
    <div className={'col-xl-6 col-lg-6 col-md-12 CardCategory__Link'}>
      <Link
        to={props.link}
        onClick={props.action}
      >
        <div className={'CardCategory__root'}>
          <div className={'CardCategory__container row justify-content-center align-items-center'}>
            <div className={'col-4'}>
              <img className={'CardCategory__image'} src={props.image} alt={props.title} />
            </div>
            <div className={'col-8'}>
              <span className={'CardCategory__title'}>{props.title}</span>
            </div>
          </div>
        </div>
      </Link>
    </div>
  );
};
