import React, {Component} from "react";
import {Link} from 'react-router-dom';
import './styles/CardSelectorItem.css';
import {connect} from "react-redux";
import Cookies from "js-cookie";

class CardSelectorItem extends Component {
  render() {
    if (this.props.title === 'Perros' || this.props.title === 'Gatos') {
      return (
        <div className={'col-xl-4 col-lg-4 col-md-4 col-sm-12 CardSelectorItem__main'}>
          <Link to={this.props.link} onClick={() => {
            Cookies.set('hachikopets_pet', this.props.title, { expires: 7, path: '/' });
            this.props.handlePet(Cookies.get('hachikopets_pet'));
          }}>
            <div className={'CardSelectorItem__root'} style={{ backgroundImage: `url(${this.props.image})` }}>
              <div className={'CardSelectorItem__container'}>
                <span className={'CardSelectorItem__title'}>{this.props.title}</span>
              </div>
            </div>
          </Link>
        </div>
      );
    } else {
      return (
        <div className={'col-xl-4 col-lg-4 col-md-4 col-sm-12 CardSelectorItem__main'}>
          <Link to={this.props.link}>
            <div className={'CardSelectorItem__root'} style={{ backgroundImage: `url(${this.props.image})` }}>
              <div className={'CardSelectorItem__container'}>
                <span className={'CardSelectorItem__title'}>{this.props.title}</span>
              </div>
            </div>
          </Link>
        </div>
      );
    }
  }
}

const mapStateToProps = state => ({
  location: state.location,
  pet: state.pet,
});

const mapDispatchToProps = dispatch => ({
  handlePet(pet){
    dispatch({
      type: 'CHANGE_PET',
      pet
    })
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(CardSelectorItem);
