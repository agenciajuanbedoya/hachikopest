import React from "react";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import "./styles/CardService.css";

const CardService = (props) => {
  return (
    <div className={'col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12 col-12'}>
      <Link to={props.link}>
        <div className={'CardService__container'} style={{ backgroundImage: `url(${props.background})` }}>
          <div className={'CardService__container-text'}>
            <div className={'CardService__container-text-content'}>
              {props.icon}
              <h2 className={'CardService__container-title'}>{props.title}</h2>
            </div>
          </div>
        </div>
      </Link>
    </div>
  );
};

const mapStateToProps = state => ({
  location: state.location
});

const mapDispatchToProps = dispatch => ({
  // handleCity(city){
  //   dispatch({
  //     type: 'CHANGE_CITY',
  //     city
  //   })
  // }
});

export default connect(mapStateToProps, mapDispatchToProps)(CardService);
