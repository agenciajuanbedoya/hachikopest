import React, {Component} from "react";
import { Link } from "react-router-dom";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
// import {faFire} from "@fortawesome/free-solid-svg-icons";
import {faShoppingCart} from "@fortawesome/free-solid-svg-icons";

import './styles/CardLastProduct.css';

class CardLastProduct extends Component {
  formatPeso = new Intl.NumberFormat('es-CO', {
    style: 'currency',
    currency: 'COP',
    minimumFractionDigits: 0
  });

  render() {
    return (
      <div className={'col-12 CardLastProduct__root'}>
        <div className={'row justify-content-center'}>
          <div className={'col-11 CardLastProduct__header'}>
            <img
              className={'CardLastProduct__image'}
              src={this.props.image}
              alt={this.props.title} />
            <div className={'CardLastProduct__options'}>
              <div className={'CardLastProduct__options-container'}>
                <h2 className={'CardLastProduct__options-title'}>{this.props.title}</h2>
                <p className={'CardLastProduct__options-subtitle'}>{this.props.subtitle}</p>
              </div>
            </div>
          </div>
          <div className={'col-11 CardLastProduct__content'}>
            <Link to={"/product/"+this.props.id}>
              <div className={'row justify-content-between align-items-center'}>
                <div className={'col-auto'}>
                  <div className={'CardLastProduct__content-title'}>
                    Desde:
                  </div>
                  <div className={'CardLastProduct__content-price'}>
                    {this.formatPeso.format(this.props.price)}
                  </div>
                </div>
                <div className={'col-auto'}>
                  <div className={'CardLastProduct__content-btn'}>
                    <FontAwesomeIcon className={'Header-Top__icon'} icon={faShoppingCart} />
                  </div>
                </div>
              </div>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default CardLastProduct;
