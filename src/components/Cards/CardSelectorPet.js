import React, {Component} from "react";
import {Link} from 'react-router-dom';
import './styles/CardSelectorPet.css';
import {connect} from "react-redux";

class CardSelectorPet extends Component {
  render() {
    return (
      <div className={'CardSelectorPet__main'}>
        <Link to={'#'} onClick={() => this.props.handlePet(this.props.title)}>
          <div className={'CardSelectorPet__root'} style={{ backgroundImage: `url(${this.props.image})` }}>
            <div className={'CardSelectorPet__container'}>
              <span className={'CardSelectorPet__title'}>{this.props.title}</span>
            </div>
          </div>
        </Link>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  pet: state.pet
});

const mapDispatchToProps = dispatch => ({
  handlePet(pet){
    dispatch({
      type: 'CHANGE_PET',
      pet
    })
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(CardSelectorPet);
