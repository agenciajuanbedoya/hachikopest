import React, {Component} from 'react';
import OwlCarousel from 'react-owl-carousel';
import {connect} from "react-redux";
import Cookies from "js-cookie";
import CardProductSlider from "../../Cards/CardProductSlider";
import API from "../../../core/Api";
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import '../Elements/Styles/ProductCarousel.css';

class ProductCarousel extends Component{
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      docs: [],
      total: 0,
      limit: 0,
      page: 0,
      pages: 0,
    }
  }

  fetchProducts = async () => {
    const res = await fetch(API + `/products?location=${Cookies.get('hachikopets_loc').toLowerCase()}&pet=${this.props.pet}&page=1`);
    const data = await res.json();
    console.log(data.docs);
    this.setState({
      ...this.state,
      docs: data.docs,
      total: data.total,
      limit: data.limit,
      page: data.page,
      pages: data.pages,
    })
  };

    render() {
        return (
            <OwlCarousel
              className="owl-theme row"
              loop={true}
              margin={0}
              autoplay={true}
              responsive={{
                0: {
                  items: 1
                },
                480: {
                  items: 1
                },
                767: {
                  items: 2
                },
                979: {
                  items: 3
                },
                1200: {
                  items: 4
                }
              }}
            >
              {this.state.docs.map(products => (<CardProductSlider data={products} key={products.id} />))}
            </OwlCarousel>
        );
    }

  componentDidMount() {
    this.fetchProducts().then(r => {
      this.setState({
        ...this.state,
        loading: false,
      });
    });
  }
}

const mapStateToProps = state => ({
  pet: state.pet,
  location: state.location
});

export default connect(mapStateToProps)(ProductCarousel);
