import React, {Component} from 'react';

import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import '../Elements/Styles/BrandsCarousel.css';

class BrandsCarousel extends Component{
  render() {
    return (
      <OwlCarousel
        className="owl-theme"
        items={6}
        responsive={{
          0: {
            items: 1
          },
          200: {
            items: 2
          },
          600: {
            items: 3
          },
          1000: {
            items: 6
          }
        }}
        loop
        margin={20}
        nav={false}
        autoplay={true} >
        <div className="item">
          <img
            src={'https://seeklogo.com/images/P/purina-logo-10CD51CD8D-seeklogo.com.png'}
            alt="Purina" height={'100px'} width={'100px'} />
        </div>
        <div className="item">
          <img
            src={'https://www.hillspet.es/content/dam/cp-sites/hills/hills-pet/global/general/logos/2018-homepage-refresh/Hills_TransformingLives_Logo_RGB_GLOBAL.png'}
            alt="Purina" height={'100px'} width={'100px'} />
        </div>
        <div className="item">
          <img
            src={'https://www.codegar.com/wp-content/uploads/2017/11/oh-mai-gat-300x250.png'}
            alt="Purina" height={'100px'} width={'100px'} />
        </div>
      </OwlCarousel>
    );
  }
}

export default BrandsCarousel;
