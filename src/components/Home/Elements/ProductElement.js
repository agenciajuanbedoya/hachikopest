import React, {Component} from "react";
import './Styles/ProductElement.css';

class ProductElement extends Component {
    render() {
        return (
            <div className={'col-4 ProductElement__container'}>
                <div className={'ProductElement__main'}>
                    <img className={'ProductElement__image'} src={'https://i.picsum.photos/id/209/536/354.jpg'} width={'100%'} alt=""/>
                    <div className={'row ProductElement__content'}>
                        <div className={'col-12 ProductElement__title'}>TARJETA DE MEMBRESÍA</div>
                        <div className={'col-12 ProductElement__resume'}>HACHIKO PETS</div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ProductElement;
