import React, {Component} from "react";
import LeftImage from '../../../images/icons/perro1.png';
import RightImage from '../../../images/icons/gato1.png';
import BrandsCarousel from "./BrandsCarousel";
import './Styles/BrandSlider.css';

class BrandSlider extends Component {
    render() {
        return (
            <div className={'BrandSlider__main'}>
                <div className={'row align-items-center'}>
                    <div className={'col-xl-2 col-lg-2 col-md-2 BrandSlider__image-container'}>
                        <img className={'BrandSlider__left'} src={LeftImage} width={'100%'} alt=""/>
                    </div>
                    <div className={'col-xl-8 col-lg-8 col-md-8'}>
                        <div className={'BrandSlider__container'}>
                            <BrandsCarousel />
                        </div>
                    </div>
                    <div className={'col-xl-2 col-lg-2 col-md-2'}>
                        <img className={'BrandSlider__right'} src={RightImage} width={'100%'} alt=""/>
                    </div>
                </div>
            </div>
        );
    }
}

export default BrandSlider;
