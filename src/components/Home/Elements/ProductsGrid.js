import React, {Component} from 'react';

import API from "../../../core/Api";
import CardProduct from "../../Cards/CardProduct";

import '../Elements/Styles/ProductCarousel.css';

class ProductsGrid extends Component{
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      products: [],
      pageCount: 0,
      itemCount: 0,
      pages: [],
    }
  }

  fetchData = async () => {
    const res = await fetch(API + "/products/by/type/simple?page=1&limit=16");
    const data = await res.json();
    this.setState({
      products: data.products,
      pageCount: data.pageCount,
      itemCount: data.itemCount,
      pages: data.pages,
    })
  };

  render() {
    return (
      <section className={'container'}>
        <div className={'row'}>
          {this.state.products.map(products => (<CardProduct data={products} />))}
        </div>
      </section>
    );
  }

  componentDidMount() {
    this.fetchData().then(r => {
      this.setState({ loading: false });
      console.log(this.state.products)
    });
  }
}

export default ProductsGrid;
