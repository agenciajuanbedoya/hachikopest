import React, {Component} from "react";
import {Carousel} from "react-bootstrap";

import './Styles/SliderMain.css';

import DemoImg from '../../../images/gallery/1.png'

class SliderMain extends Component {
    render() {
        return (
            <div className={'SliderMain__container'}>
                <Carousel touch={true} >
                    <Carousel.Item>
                        <img className="d-block w-100" src={DemoImg} alt="First slide"/>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img className="d-block w-100" src={DemoImg} alt="First slide"/>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img className="d-block w-100" src={DemoImg} alt="First slide"/>
                    </Carousel.Item>
                </Carousel>
            </div>
        );
    }
}

export default SliderMain;
