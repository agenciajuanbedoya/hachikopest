import React, {Component} from "react";
import './Styles/Services.css';

class Services extends Component {
    render() {
        return (
            <div className={'Services__container row align-items-center'}>
                <div className={'col-auto'}>
                    <img className={'Services__icon'} src={this.props.image} alt={this.props.text} />
                </div>
                <div className={'col'}>
                    <span className={'Services__text'}>{this.props.text}</span>
                </div>
            </div>
        );
    }
}

export default Services
