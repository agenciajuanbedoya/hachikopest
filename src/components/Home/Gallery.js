import React, {Component} from "react";
import OwlCarousel from 'react-owl-carousel';
import Cookies from "js-cookie";
import LanzIMG1 from '../../images/gallery/1.png';
import LanzIMG2 from '../../images/gallery/lanzamiento-general.png';
import LanzIMGMedellin from '../../images/gallery/lanzamiento-medellin.png';
import LanzIMGBucaramanga from '../../images/gallery/lanzamiento-bucaramanga.png';
import LanzIMGBogota from '../../images/gallery/lanzamiento-bogota.png';

import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import './Styles/Gallery.css';

class Gallery extends Component {
  render() {
    if (Cookies.get('hachikopets_loc').toLowerCase() === "medellin") {
      return (
        <div className={'container-fluid animated fadeInUp fast'}>
          <div className={'row justify-content-center'} >
            <div className={'col-12 Gallery__container'}>
              <OwlCarousel
                className="owl-theme"
                items={1}
                loop={true}
                autoplay={true}>
                <div className="item">
                  <img
                    src={LanzIMGMedellin}
                    alt="Ahora disponible en Medellín"
                    width={'100%'}
                  />
                </div>
                <div className="item">
                  <img
                    src={LanzIMG2}
                    alt="Más productos y envios a toda la ciudad"
                    width={'100%'}
                  />
                </div>
                <div className="item">
                  <img
                    src={LanzIMG1}
                    alt="Pide y recibe cuando quieras"
                    width={'100%'}
                  />
                </div>
              </OwlCarousel>
            </div>
          </div>
        </div>
      );
    } else if (Cookies.get('hachikopets_loc').toLowerCase() === "bucaramanga") {
      return (
        <div className={'container-fluid animated fadeInUp fast'}>
          <div className={'row justify-content-center'} >
            <div className={'col-12 Gallery__container'}>
              <OwlCarousel
                className="owl-theme"
                items={1}
                loop={true}
                autoplay={true}>
                <div className="item">
                  <img
                    src={LanzIMGBucaramanga}
                    alt="Ahora disponible en Bucaramanga"
                    width={'100%'}
                  />
                </div>
                <div className="item">
                  <img
                    src={LanzIMG2}
                    alt="Más productos y envios a toda la ciudad"
                    width={'100%'}
                  />
                </div>
                <div className="item">
                  <img
                    src={LanzIMG1}
                    alt="Pide y recibe cuando quieras"
                    width={'100%'}
                  />
                </div>
              </OwlCarousel>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className={'container-fluid animated fadeInUp fast'}>
          <div className={'row justify-content-center'} >
            <div className={'col-12 Gallery__container'}>
              <OwlCarousel
                className="owl-theme"
                items={1}
                loop={true}
                autoplay={true}>
                <div className="item">
                  <img
                    src={LanzIMGBogota}
                    alt="Ahora disponible en Bogotá"
                    width={'100%'}
                  />
                </div>
                <div className="item">
                  <img
                    src={LanzIMG2}
                    alt="Más productos y envios a toda la ciudad"
                    width={'100%'}
                  />
                </div>
                <div className="item">
                  <img
                    src={LanzIMG1}
                    alt="Pide y recibe cuando quieras"
                    width={'100%'}
                  />
                </div>
              </OwlCarousel>
            </div>
          </div>
        </div>
      );
    }
  }
}

export default Gallery;
