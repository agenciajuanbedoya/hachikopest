import React, {Component} from "react";
import Swiper from 'react-id-swiper';
import CardLastProduct from "../Cards/CardLastProduct";
import TitleSubtitle from "../Titles/TitleSubtitle";
import Image from '../../images/nubes.png';
import 'swiper/css/swiper.css'
import './Styles/LastProduct.css';

class LastProduct extends Component {
  render() {
    const params = {
      pagination: '.swiper-pagination',
      effect: 'coverflow',
      grabCursor: false,
      loop: true,
      centeredSlides: true,
      slidesPerView: 'auto',
      coverflow: {
        rotate: 10,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows : true
      },
      autoplay: {
        delay: 2500,
        disableOnInteraction: false
      },
    };

    return (
      <div className={'LastProduct__container'} style={{ backgroundImage: `url(${Image})` }}>
        <div className={'container'}>
          <TitleSubtitle
            title={'Un mundo para tu mascota'}
            subtitle={'Los mejores productos y servicios'}
          />
          <div className={'row'}>
            <div className={'col-12'}>
              <Swiper {...params}>
                <div className={'swiper-slider'}>
                  <CardLastProduct
                    key={0}
                    image={'https://www.hachikopets.com/wp-content/uploads/2019/08/tarjeta-de-membresia.jpg'}
                    title={'Tarjeta de membresía'}
                    subtitle={'Hachiko Pets'}
                    id={'3463'}
                    price={'60000'}
                  />
                </div>
                <div className={'swiper-slider'}>
                  <CardLastProduct
                    key={1}
                    image={'https://www.hachikopets.com/wp-content/uploads/2019/08/tarjeta-de-membresia.jpg'}
                    title={'Tarjeta de membresía'}
                    subtitle={'Hachiko Pets'}
                    id={'3463'}
                    price={'60000'}
                  />
                </div>
                <div className={'swiper-slider'}>
                  <CardLastProduct
                    key={2}
                    image={'https://www.hachikopets.com/wp-content/uploads/2019/08/tarjeta-de-membresia.jpg'}
                    title={'Tarjeta de membresía'}
                    subtitle={'Hachiko Pets'}
                    id={'3463'}
                    price={'60000'}
                  />
                </div>
              </Swiper>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LastProduct;
