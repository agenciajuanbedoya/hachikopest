import React, {Component} from "react";
import ProductCarousel from "./Elements/ProductCarousel";
import Title from "../Titles/Title";
import './Styles/Trending.css';

class Trending extends Component {
    render() {
        return (
            <div className={'container Trending__container'}>
                <div className={'row'}>
                    <div className={'col-12'}>
                        <Title text={'Destacados'} />
                    </div>
                    <ProductCarousel />
                </div>
            </div>
        );
    }
}

export default Trending;
