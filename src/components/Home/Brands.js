import React, {Component} from "react";
import BrandSlider from "./Elements/BrandSlider";
import TitleSubtitle from "../Titles/TitleSubtitle";
import './Styles/Brands.css';

class Brands extends Component {
  render() {
    return (
      <div className={'Brands__container'}>
        <div className={'row align-items-center justify-content-center'}>
          <TitleSubtitle
            title={'Traemos las mejores marcas para ti'}
            subtitle={'Respaldados por los más grandes nombres del mercado'}
          />
        </div>
        <BrandSlider />
      </div>
    );
  }
}

export default Brands;
