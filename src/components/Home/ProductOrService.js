import React, {Component} from "react";
import CardSelectorItem from "../Cards/CardSelectorItem";
import imgServicios from '../../images/selector/servicios.png';
import imgPerro from '../../images/selector/perro.png';
import imgGatos from '../../images/selector/gatos.png';
import './Styles/ProductOrService.css';

class ProductOrService extends Component{
    render() {
        return (
            <div className={'container ProductOrService__container'}>
                <div className={'row'}>
                    <CardSelectorItem title={'Perros'} image={imgPerro} link={'products'} />
                    <CardSelectorItem title={'Servicios'} image={imgServicios} link={'services'} />
                    <CardSelectorItem title={'Gatos'} image={imgGatos} link={'products'} />
                </div>
            </div>
        );
    }
}

export default ProductOrService;
