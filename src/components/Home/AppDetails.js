import React, {Component} from "react";
import {Link} from "react-router-dom";
import LogoAndroid from '../../images/icons/disponible-en-google-play-badge-300x89.png';
import ScreenApp from '../../images/6-productos.png';
import LogoApp from '../../images/icons/conoce-nuestra-app.png';
import LogoiOS from '../../images/disponible-ios.png';

import './Styles/AppDetails.css';

class AppDetails extends Component {
    render() {
        return (
            <div className={'AppDetails__container container'}>
                <div className={'row align-items-center'}>
                    <div className={'col-xl-6 order-xl-1 col-lg-6 order-lg-1 col-md-6 order-md-1 order-sm-2 order-2'}>
                        <img src={LogoApp} width={'80%'} alt=""/>
                        <p className={'AppDetails__text'}>Es un placer presentartenuestra aplicación móvil.
                            Disponible para los principales sistema operativos (Android & iOS).</p>
                        <Link to={'#'}>
                            <img className={'AppDetails__android-logo'} src={LogoAndroid} alt={'Google Play Store'} />
                        </Link>
                        <Link to={'#'}>
                            <img className={'AppDetails__ios-logo'} src={LogoiOS} alt={'Apple App Store'} />
                        </Link>
                    </div>
                    <div className={'col-xl-6 order-xl-2 col-lg-6 order-lg-2 col-md-6 order-md-2 order-sm1 order-1'}>
                        <div className={'text-center'}>
                            <img src={ScreenApp} width={'80%'} alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AppDetails;
