import React, {Component} from "react";
import {connect} from "react-redux";
import Services from "./Elements/Services";
import bgImage from '../../images/bg-details.jpg';
import Image1 from '../../images/icons/programa-tus-compras.png';
import Image2 from '../../images/icons/envios-seguros-a-bogota.png';
import Image3 from '../../images/icons/entregas-el-mismo-dia.png';
import Image4 from '../../images/icons/mas-medios-de-pago.png';
import Image5 from '../../images/icons/pago-contra-entrega.png';
import Logo from '../../images/icons/Logo-Hachiko-pets3bLANCO.png'
import './Styles/ServicesDetails.css';

class ServicesDetails extends Component {
  render() {
    return (
      <div className={'ServicesDetails__main'} style={{ backgroundImage: `url(${bgImage})` }}>
        <div className={'ServicesDetails__container container'}>
          <div className={'row justify-container-center align-items-center'}>
            <div className={'col-xl-8 col-lg-8 col-md-8 col-sm-12'}>
              <div className={'row'}>
                <div className={'col-12'}>
                  <img src={Logo} width={'80%'} alt={'Hachiko Pets'} />
                </div>
              </div>
              <Services text={'Programa tus compras'} image={Image1} />
              <Services text={'Más medios de pago'} image={Image4} />
              <Services text={'Pago contraentrega'} image={Image5} />
              <Services text={'Entregas rápidas'} image={Image3} />
              <Services text={`Envíos seguros`} image={Image2} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  pet: state.pet,
  location: state.location
});

export default connect(mapStateToProps)(ServicesDetails);
