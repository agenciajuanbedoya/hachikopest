import React, {Component} from "react";
import bgImage from "../../images/bg-category.png";
import '../Selector/style/index.css';

class CategorySection extends Component {
  render() {
    return (
      <section className={'container CategorySelector__root animated fadeInUp fast'}>
        <div className={'row CategorySelector__container'} style={{ backgroundImage: `url(${bgImage})` }}>
          <div className={'col-xl-8 col-lg-8 col-md-12'}>
            <h1 className={'CategorySelector__title text-center'}>{this.props.attr} para tu mascota</h1>
          </div>
        </div>
      </section>
    );
  }
}

export default CategorySection;
