import React, {Component} from "react";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faMinus, faPlus, faTrash} from "@fortawesome/free-solid-svg-icons";
import './styles/ShoppingOrders.css';

const Product = (props) => {
  const formatPeso = new Intl.NumberFormat('es-CO', {
    style: 'currency',
    currency: 'COP',
    minimumFractionDigits: 0
  });
  return(
    <li className="list-group-item d-flex justify-content-between align-items-center lh-condensed">
      <div>
        <h6 className="my-0">{props.data.title}</h6>
        <small className="text-muted">Marca: {props.data.tag} | Sucursal: {props.location}</small>
      </div>
      <span className="ShoppingOrders__data">
        {props.data.quantity !== 1 ? (
          <div><span>{props.data.quantity}</span>x</div>
        ) : (
          <div />
        )}
        {formatPeso.format(props.data.price)}
        <div className={'ShoppingOrders__remove'}>
          <Link to={'#'} style={{color: '#00b894'}} onClick={() => {props.handleAddQuantity(props.uid)}}>
            <FontAwesomeIcon icon={faPlus} />
          </Link>
          <Link to={'#'} style={{color: '#d63031'}} onClick={() => {props.handleMinQuantity(props.uid)}}>
            <FontAwesomeIcon icon={faMinus}/>
          </Link>
          <Link to={'#'} style={{color: '#2d3436'}} onClick={() => {props.handleRemove(props.uid)}}>
            <FontAwesomeIcon icon={faTrash}/>
          </Link>
        </div>
      </span>
    </li>
  );
};












// const Discount = () => {
//   return(
//     <li className="list-group-item d-flex justify-content-between bg-light">
//       <div className="text-success">
//         <h6 className="my-0">Promo code</h6>
//         <small>EXAMPLECODE</small>
//       </div>
//       <span className="text-success">-$5</span>
//     </li>
//   );
// };

class ShoppingOrders extends Component {
  render() {
    this.formatPeso = new Intl.NumberFormat('es-CO', {
      style: 'currency',
      currency: 'COP',
      minimumFractionDigits: 0
    });
    return (
      <div className={'ShoppingOrders__main'}>
        <h4 className="mb-3">Elementos del pedido</h4>
        <ul className="list-group mb-3">
          {this.props.products.map((product, index) => (
            <Product
              data={product}
              key={index}
              uid={index}
              handleRemove={this.props.handleRemove}
              handleAddQuantity={this.props.handleAddQuantity}
              handleMinQuantity={this.props.handleMinQuantity}
              location={this.props.location}
            />
            ))}
          <li className="list-group-item d-flex justify-content-between">
            <span>Total</span>
            <strong>
              {this.formatPeso.format(this.props.totalCart)}
            </strong>
          </li>
        </ul>

        {/*<form className="card p-2">*/}
        {/*  <div className="input-group">*/}
        {/*    <input type="text" className="form-control" placeholder="Codigo promocional" />*/}
        {/*    <div className="input-group-append">*/}
        {/*      <button type="submit" className="btn btn-secondary">Canjear</button>*/}
        {/*    </div>*/}
        {/*  </div>*/}
        {/*</form>*/}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.products,
  totalCart: state.totalCart,
  location: state.location,
});

const mapDispatchToProps = dispatch => ({
  handleAddQuantity(quantity){
    dispatch({
      type: 'ADD_QUANTITY_CART',
      quantity,
    })
  },
  handleMinQuantity(quantity){
    dispatch({
      type: 'MIN_QUANTITY_CART',
      quantity,
    })
  },
  handleProducts(products){
    dispatch({
      type: 'ADD_PRODUCT',
      products
    })
  },
  handleRemove(product){
    dispatch({
      type: 'REMOVE_PRODUCT',
      product
    })
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingOrders);
