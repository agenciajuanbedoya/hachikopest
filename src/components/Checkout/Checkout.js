import React, {Component} from "react";
import {Link} from 'react-router-dom';
import {connect} from "react-redux";
import PSECheckout from "./PSECheckout";
import CARDCheckout from "./CARDCheckout";
import CASHCheckout from "./CASHCheckout";
import './styles/Checkout.css';

class Checkout extends Component {
  render() {
    if (this.props.paymentGateway === 'PSE') {
      return (
        <React.Fragment>
          <div className="row justify-content-between Checkout__main">
            <Link to={'#'} onClick={() => this.props.handlePaymentGateway('PSE')}>
              <div className="Checkout__BTN_Active">Pagos con PSE</div>
            </Link>
            <Link to={'#'} onClick={() => this.props.handlePaymentGateway('CARD')}>
              <div className="Checkout__BTN">Tarjeta de crédito</div>
            </Link>
            <Link to={'#'} onClick={() => this.props.handlePaymentGateway('CASH')}>
              <div className="Checkout__BTN">Pago en efectivo</div>
            </Link>
          </div>
          <PSECheckout />
          <div component={Link} className={'DeliverySelector__back'} onClick={() => this.props.handleResetTimezone(undefined)}>
            Regresar
          </div>
        </React.Fragment>
      );
    } else if (this.props.paymentGateway === 'CARD') {
      return (
        <React.Fragment>
          <div className="row justify-content-between Checkout__main">
            <Link to={'#'} onClick={() => this.props.handlePaymentGateway('PSE')}>
              <div className="Checkout__BTN">Pagos con PSE</div>
            </Link>
            <Link to={'#'} onClick={() => this.props.handlePaymentGateway('CARD')}>
              <div className="Checkout__BTN_Active">Tarjeta de crédito</div>
            </Link>
            <Link to={'#'} onClick={() => this.props.handlePaymentGateway('CASH')}>
              <div className="Checkout__BTN">Pago en efectivo</div>
            </Link>
          </div>
          <CARDCheckout />
          <div component={Link} className={'DeliverySelector__back'} onClick={() => this.props.handleResetTimezone(undefined)}>
            Regresar
          </div>
        </React.Fragment>
      );
    } else if (this.props.paymentGateway === 'CASH') {
      return (
        <React.Fragment>
          <div className="row justify-content-between Checkout__main">
            <Link to={'#'} onClick={() => this.props.handlePaymentGateway('PSE')}>
              <div className="Checkout__BTN">Pagos con PSE</div>
            </Link>
            <Link to={'#'} onClick={() => this.props.handlePaymentGateway('CARD')}>
              <div className="Checkout__BTN">Tarjeta de crédito</div>
            </Link>
            <Link to={'#'} onClick={() => this.props.handlePaymentGateway('CASH')}>
              <div className="Checkout__BTN_Active">Pago en efectivo</div>
            </Link>
          </div>
          <CASHCheckout />
          <div component={Link} className={'DeliverySelector__back'} onClick={() => this.props.handleResetTimezone(undefined)}>
            Regresar
          </div>
        </React.Fragment>
      );
    }
  }
}

const mapStateToProps = state => ({
  paymentGateway: state.paymentGateway,
});

const mapDispatchToProps = dispatch => ({
  handleResetTimezone(status){
    dispatch({
      type: 'RESET_TIMEZONE',
      status
    })
  },
  handlePaymentGateway(paymentGateway) {
    dispatch({
      type: 'SET_PAYMENT_GATEWAY',
      paymentGateway
    })
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Checkout);
