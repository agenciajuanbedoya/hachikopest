import React, {Component} from "react";
import {ToastsStore, ToastsContainer, ToastsContainerPosition} from 'react-toasts';
import SimpleReactValidator from 'simple-react-validator';
import uuid from 'react-uuid'
import {connect} from "react-redux";
import {LocationApi} from "../../core/Api";
import {database} from "../../core/firebase";

class PSECheckout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bank: '',
      invoice: '',
      description: '',
      value: '',
      tax: '0',
      tax_base: '0',
      currency: 'COP',
      type_person: '0',
      doc_type: 'CC',
      doc_number: '',
      name: '',
      last_name: '',
      email: '',
      country: 'CO',
      cell_phone: '',
      url_response: 'https://www.hachikopets.com/compraexitosa',
      url_confirmation: 'https://www.hachikopets.com/compraexitosa',
      method_confirmation: 'GET',
    };
    this.validator = new SimpleReactValidator({
      locale: 'es',
      element: message => <div style={{color: 'red'}}>{message}</div>,
      messages: {
        default: 'Información requerida',
      }, autoForceUpdate: this});
  };

  pushOrder = () => {
    if (this.validator.allValid()) {

      database.ref('orders')
        .child(this.props.user)
        .push()
        .set({
          type: 'PSE',
          date: new Date(),
          ref: this.props.uid,
          user: this.props.user,
          delivery: this.props.delivery,
          location: this.props.location,
          timezone: this.props.timezone,
          products: this.props.products,
          totalCart: this.props.totalCart,
          checkout: this.props,
          paymentGateway: this.props.paymentGateway,
        }).then(() => {
          ToastsStore.success("Pedido éxitoso");
          window.open("https://wwww.hachikopets.com/compraexitosa", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=600,height=600");
        });

    } else {
      this.validator.showMessages();
      ToastsStore.error("Error al registrarse");
      this.forceUpdate();
    }
  };

  fetchUser = () => {
    database.ref('users/' + this.props.uid).on('value', snap => {
      this.setState({
        ...this.state,
        doc_number: snap.val().doc_number,
        doc_type: snap.val().doc_type,
        name: snap.val().name,
        last_name: snap.val().last_name,
        email: snap.val().email,
        phone: snap.val().phone
      });
      console.log(snap.val());
    });
  };

  fetchIP = async () => {
    this.setState({
      ...this.state,
      loading: true,
    });
    const res = await fetch(LocationApi);
    const data = await res.json();
    this.setState({
      ...this.state,
      country: data.country_code,
    });
  };

  handleChange = e => {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value,
    });
  };

  handlePayment = () => {
    const myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer U2FsdGVkX1+FgvJxKZc0mT5BeGDhG2joQvtxxhobgC0=");
    myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

    const urlencoded = new URLSearchParams();
    urlencoded.append("bank", this.state.bank);
    urlencoded.append("invoice", this.state.invoice);
    urlencoded.append("description", this.state.description);
    urlencoded.append("value", this.state.value);
    urlencoded.append("tax", this.state.tax);
    urlencoded.append("tax_base", this.state.tax_base);
    urlencoded.append("currency", this.state.currency);
    urlencoded.append("type_person", this.state.type_person);
    urlencoded.append("doc_type", this.state.doc_type);
    urlencoded.append("doc_number", this.state.doc_number);
    urlencoded.append("name", this.state.name);
    urlencoded.append("last_name", this.state.last_name);
    urlencoded.append("email", this.state.email);
    urlencoded.append("country", this.state.country);
    urlencoded.append("cell_phone", this.state.cell_phone);
    urlencoded.append("url_response", this.state.url_response);
    urlencoded.append("url_confirmation", this.state.url_confirmation);
    urlencoded.append("method_confirmation", this.state.method_confirmation);

    const optionPSE = {
      method: 'POST',
      headers: myHeaders,
      body: urlencoded,
    };

    fetch("https://api.hachikopets.com/pse", optionPSE)
      .then(res => {
        if (res.status === 200) {
          res.text().then(res => {
            database.ref('orders/' + this.props.user.uid + '/' + this.state.invoice)
              .set(JSON.parse(res))
              .then(() => {
                this.props.handleCheckout(JSON.parse(res));
              })
              .then(() => {
                const result = JSON.parse(res).data.urlbanco;
                this.pushOrder();
                window.open(result, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=600,height=600");
              })
              .catch(err => {
                console.log(err);
              });
          });
        }
      }).catch(error => {
      console.log('error', error.json())
    });
  };

  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-md-6 mb-3">
            <label>Tipo de documento</label>
            <select
              className="custom-select d-block w-100"
              name="bank"
              value={this.state.bank}
              defaultValue={''}
              onChange={this.handleChange}
              required
            >
              <option value={''} disabled>Elija su banco</option>
              <option value={'1022'}>Banco Union Colombiano (Demo)</option>
              <option value={'1040'}>BANCO AGRARIO</option>
              <option value={'1052'}>BANCO AV VILLAS</option>
              <option value={'1013'}>BANCO BBVA COLOMBIA S.A.</option>
              <option value={'1032'}>BANCO CAJA SOCIAL</option>
              <option value={'1066'}>BANCO COOPERATIVO COOPCENTRAL</option>
              <option value={'1051'}>BANCO DAVIVIENDA</option>
              <option value={'1001'}>BANCO DE BOGOTA</option>
              <option value={'1023'}>BANCO DE OCCIDENTE</option>
              <option value={'1062'}>BANCO FALABELLA</option>
              <option value={'1012'}>BANCO GNB SUDAMERIS</option>
              <option value={'1006'}>BANCO ITAU</option>
              <option value={'1060'}>BANCO PICHINCHA S.A.</option>
              <option value={'1002'}>BANCO POPULAR</option>
              <option value={'1058'}>BANCO PROCREDIT</option>
              <option value={'1065'}>BANCO SANTANDER COLOMBIA</option>
              <option value={'1069'}>BANCO SERFINANZA</option>
              <option value={'1007'}>BANCOLOMBIA</option>
              <option value={'1061'}>BANCOOMEVA S.A.</option>
              <option value={'1283'}>CFA COOPERATIVA FINANCIERA</option>
              <option value={'1009'}>CITIBANK</option>
              <option value={'1292'}>CONFIAR COOPERATIVA FINANCIERA</option>
              <option value={'1551'}>DAVIPLATA</option>
              <option value={'1507'}>NEQUI</option>
              <option value={'1151'}>RAPPIPAY</option>
              <option value={'1019'}>SCOTIABANK COLPATRIA</option>
            </select>
          </div>

          <div className="col-md-6 mb-3">
            <label>Tipo de documento</label>
            <select
              className="custom-select d-block w-100"
              name="doc_type"
              value={this.state.doc_type}
              onChange={this.handleChange}
              defaultValue={''}
              required
            >
              <option value={''} disabled>Elija su tipo de decumento</option>
              <option value={'CC'}>Cédula de ciudadanía</option>
              <option value={'CE'}>Cédula de extrangería</option>
              <option value={'DNI'}>Identificación extranjera</option>
              <option value={'PPN'}>Pasaporte</option>
              <option value={'NIT'}>NIT</option>
            </select>
          </div>
          <div className="col-md-6 mb-3">
            <label>Número de documento</label>
            <input
              type="number"
              className="form-control"
              onChange={this.handleChange}
              value={this.state.doc_number}
              name="doc_number"
              required
            />
            {this.validator.message('doc_number', this.state.doc_number, 'required')}
          </div>
          <div className="col-md-6 mb-3">
            <label>Nombre</label>
            <input
              type="text"
              className="form-control"
              onChange={this.handleChange}
              value={this.state.name}
              name="name"
              required
            />
            {this.validator.message('name', this.state.name, 'required')}
          </div>
          <div className="col-md-6 mb-3">
            <label>Apellidos</label>
            <input
              type="text"
              className="form-control"
              name="last_name"
              onChange={this.handleChange}
              value={this.state.last_name}
              required
            />
            {this.validator.message('last_name', this.state.last_name, 'required')}
          </div>
          <div className="col-md-6 mb-3">
            <label>Teléfono</label>
            <input
              type="phone"
              className="form-control"
              name="cell_phone"
              onChange={this.handleChange}
              value={this.state.cell_phone}
              required
            />
            {this.validator.message('cell_phone', this.state.cell_phone, 'required')}
          </div>
          <div className="col-md-6 mb-3">
            <label>Correo electrónico</label>
            <input
              type="email"
              className="form-control"
              name="email"
              onChange={this.handleChange}
              value={this.state.email}
              required
            />
            {this.validator.message('email', this.state.email, 'required')}
          </div>
          <div className="col-md-6 mb-3">
            <label>Nota</label>
            <input
              type="text"
              className="form-control"
              name="description"
              onChange={this.handleChange}
              value={this.state.description}
              required
            />
            {this.validator.message('description', this.state.description, 'required')}
          </div>
        </div>
        <ToastsContainer position={ToastsContainerPosition.BOTTOM_CENTER} store={ToastsStore}/>
        <div className="row">
          <div className="col-12">
            <input
              className={"btn btn-primary btn-lg btn-block"}
              value={'Procesar pago'}
              type={"submit"}
            />
          </div>
        </div>
      </React.Fragment>
    );
  }
  componentDidMount() {
    const invoice = uuid();
    this.fetchIP();
    this.fetchUser();
    this.setState({
      ...this.state,
      value: this.props.totalCart,
      invoice: invoice
    });
  }
  componentWillUnmount() {
    this.fetchIP();
    this.fetchUser();
  }
}

const mapStateToProps = state => ({
  uid: state.user.uid,
  user: state.user,
  delivery: state.delivery,
  location: state.location,
  timezone: state.timezone,
  totalCart: state.totalCart,
  products: state.products,
  paymentGateway: state.paymentGateway,
});

export default connect(mapStateToProps)(PSECheckout);
