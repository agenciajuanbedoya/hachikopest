import React, {Component} from "react";
import {ToastsContainer, ToastsContainerPosition, ToastsStore} from "react-toasts";
import SimpleReactValidator from "simple-react-validator";
import {connect} from "react-redux";
import DeliverySelector from "../Selector/DeliverySelector";
import {database} from "../../core/firebase";

class CASHCheckout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      last_name: '',
      doc_number: '',
      phone: '',
      email: '',
      address: '',
      address2: '',
    };
    this.validator = new SimpleReactValidator({
      locale: 'es',
      element: message => <div style={{color: 'red'}}>{message}</div>,
      messages: {
        default: 'Información requerida',
      }, autoForceUpdate: this});
  }

  fetchUserInfo = () => {
    database.ref('users/' + this.props.uid).on('value', snap => {
      this.setState({
        ...this.state,
        name: snap.val().name,
        last_name: snap.val().last_name,
        doc_number: snap.val().doc_number,
        phone: snap.val().phone,
        email: snap.val().email,
        address: snap.val().address,
        address2: snap.val().address2,
      });
    });
  };

  handleChangeDelivery = e => {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value,
    });
  };

  handleDelivery = e => {
    e.preventDefault();
    if (this.validator.allValid()) {
      this.props.handleDelivery(this.state)
    } else {
      this.validator.showMessages();
      ToastsStore.error("Falta información");
      this.forceUpdate();
    }
  };

  render() {
    if (this.props.delivery === undefined) {
      return (
        <React.Fragment>
          <form className="needs-validation" noValidate onSubmit={this.handleDelivery}>
            <h4 className="mb-3">Información de envio</h4>
            <div className="row">
              <div className="col-md-6 mb-3">
                <label>Nombre</label>
                <input
                  type="text"
                  className="form-control"
                  onChange={this.handleChangeDelivery}
                  value={this.state.name}
                  name="name"
                  required
                />
                {this.validator.message('name', this.state.name, 'required')}
              </div>
              <div className="col-md-6 mb-3">
                <label>Apellidos</label>
                <input
                  type="text"
                  className="form-control"
                  name="last_name"
                  onChange={this.handleChangeDelivery}
                  value={this.state.last_name}
                  required
                />
                {this.validator.message('last_name', this.state.last_name, 'required')}
              </div>
              <div className="col-md-6 mb-3">
                <label>Cédula o identificación</label>
                <input
                  type="number"
                  className="form-control"
                  name="doc_number"
                  onChange={this.handleChangeDelivery}
                  value={this.state.doc_number}
                />
                {this.validator.message('doc_number', this.state.doc_number, 'required')}
              </div>
              <div className="col-md-6 mb-3">
                <label>Nro. de teléfono</label>
                <input
                  type="phone"
                  className="form-control"
                  name="phone"
                  onChange={this.handleChangeDelivery}
                  value={this.state.phone}
                />
                {this.validator.message('phone', this.state.phone, 'required')}
              </div>
              <div className="col-md-6 mb-3">
                <label>Correo electrónico</label>
                <input
                  type="email"
                  className="form-control"
                  name="email"
                  onChange={this.handleChangeDelivery}
                  value={this.state.email}
                />
                {this.validator.message('email', this.state.email, 'required')}
              </div>
              <div className="col-md-6 mb-3">
                <label>Dirección</label>
                <input
                  type="text"
                  className="form-control"
                  name="address"
                  onChange={this.handleChangeDelivery}
                />
                {this.validator.message('address', this.state.address, 'required')}
              </div>
            </div>
            <div className="row">
              <div className="col-12 mb-3">
                <label>Barrio, comuna, distrito o urbanización</label>
                <input
                  type="text"
                  className="form-control"
                  name="address2"
                  onChange={this.handleChangeDelivery}
                  value={this.state.address2}
                />
                {this.validator.message('address2', this.state.address2, 'required')}
              </div>
            </div>
            <div className="row">
              <div className="col-12 mb-3">
                <label>Nota de entrega</label>
                <input
                  type="text"
                  className="form-control"
                  name="note"
                  onChange={this.handleChangeDelivery}
                  value={this.state.note}
                />
              </div>
            </div>

            <ToastsContainer position={ToastsContainerPosition.BOTTOM_CENTER} store={ToastsStore}/>
            <div className="row">
              <div className="col-12 mb-3">
                <input
                  className={"btn btn-primary btn-lg btn-block"}
                  value={'Guardar'}
                  type={"submit"}
                />
              </div>
            </div>
          </form>
        </React.Fragment>
      );
    } else {
      return <DeliverySelector />;
    }
  }

  componentDidMount() {
    this.fetchUserInfo();
  }

  componentWillUnmount() {
    this.fetchUserInfo();
  }
}

const mapStateToProps = state => ({
  delivery: state.delivery,
  uid: state.user.uid,
});

const mapDispatchToProps = dispatch => ({
  handleDelivery(delivery){
    dispatch({
      type: 'SET_DELIVERY',
      delivery
    })
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(CASHCheckout);
