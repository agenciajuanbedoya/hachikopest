import React, {Component} from "react";
import NumberFormat from 'react-number-format';
import {connect} from "react-redux";
import {database} from "../../core/firebase";
import {ToastsContainer, ToastsContainerPosition, ToastsStore} from "react-toasts";
import SimpleReactValidator from "simple-react-validator";

class CARDCheckout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      request: {
        card: {
          number: '',
          exp_year: '',
          exp_month: '',
          cvc: '',
        },
        client: {
          token_card: '',
          name: '',
          email: '',
          phone: '',
          default: true,
        },
        payment: {
          doc_type: 'CC',
          doc_number: '',
          description: '',
          value: '',
          dues: '',
        },
      },
      response: {
        card: {},
        client: {},
      },
    };
    this.validator = new SimpleReactValidator({
      locale: 'es',
      element: message => <div style={{color: 'red'}}>{message}</div>,
      messages: {
        default: 'Información requerida',
      }, autoForceUpdate: this});
  }

  pushOrder = () => {
    database.ref('orders')
      .child(this.props.uid)
      .push()
      .set({
        type: 'CARD',
        date: new Date(),
        ref: this.props.uid,
        user: this.props.user,
        delivery: this.props.delivery,
        location: this.props.location,
        timezone: this.props.timezone,
        products: this.props.products,
        totalCart: this.props.totalCart,
        checkout: this.props,
        paymentGateway: this.props.paymentGateway,
      })
      .then(() => {
        ToastsStore.success('¡Orden éxitosa!');
        window.open("https://wwww.hachikopets.com/compraexitosa", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=600,height=600");
    }).catch(err => {
      ToastsStore.error(err.message);
    })
  };

  fetchUserInfo = () => {
    database.ref('users/' + this.props.uid).on('value', snap => {
      this.setState({
        ...this.state,
        request: {
          ...this.state.request,
          client: {
            ...this.state.request.client,
            name: `${snap.val().name} ${snap.val().last_name}`,
            email: snap.val().email,
            phone: snap.val().phone,
          },
          payment: {
            ...this.state.request.payment,
            doc_type: snap.val().doc_type,
            doc_number: snap.val().doc_number,
            value: this.props.totalCart,
          },
        },
      });
    });
  };

  handleChangeCARD = e => {
    this.setState({
      ...this.state,
      request: {
        ...this.state.request,
        card: {
          ...this.state.request.card,
          [e.target.name]: e.target.value,
        },
      },
    });
  };

  handleChangeCLIENT = e => {
    this.setState({
      ...this.state,
      request: {
        ...this.state.request,
        client: {
          ...this.state.request.client,
          [e.target.name]: e.target.value,
        },
      },
    });
  };

  handleChange = e => {
    this.setState({
      ...this.state,
      request: {
        ...this.state.request,
        payment: {
          ...this.state.request.payment,
          [e.target.name]: e.target.value,
        },
      },
    });
  };

  handlePaymentCARD = e => {
    e.preventDefault();
    if (this.validator.allValid()) {
      ToastsStore.info("Procesando pago");

      const myHeaders = new Headers();
      myHeaders.append("Authorization", "Bearer U2FsdGVkX1+FgvJxKZc0mT5BeGDhG2joQvtxxhobgC0=");
      myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

      const tmpNumber = this.state.request.card.number.split(" ");
      const number = tmpNumber[0] + tmpNumber[1] + tmpNumber[2] + tmpNumber[3];

      const urlencoded = new URLSearchParams();
      urlencoded.append("number", parseInt(number).toString());
      urlencoded.append("exp_year", this.state.request.card.exp_year);
      urlencoded.append("exp_month", this.state.request.card.exp_month);
      urlencoded.append("cvc", this.state.request.card.cvc);

      const requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: urlencoded,
      };

      fetch("https://api.hachikopets.com/card", requestOptions)
        .then(res => {
          if (res.status === 200) {
            res.text().then(res => {
              const resJSON = JSON.parse(res);

              database.ref('gateways/cards')
                .child(this.props.user.uid)
                .push()
                .set(resJSON)
                .then(() => {
                  this.setState({
                    ...this.state,
                    response: {
                      ...this.state.response,
                      card: resJSON,
                    },
                  });
                })
                .then(() => {
                  ToastsStore.info("Procesando tarjeta");
                  this.handlePaymentCLIENT(resJSON.id);
                }).catch(err => {
                  ToastsStore.error(err.message);
              });

            });
          }
        }).catch(err => {
          ToastsStore.error(err.message);
      });

    } else {
      this.validator.showMessages();
      ToastsStore.error("Datos insuficientes");
      this.forceUpdate();
    }
  };

  handlePaymentCLIENT = token_card => {
    const myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer U2FsdGVkX1+FgvJxKZc0mT5BeGDhG2joQvtxxhobgC0=");
    myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

    const urlencoded = new URLSearchParams();
    urlencoded.append("token_card", token_card);
    urlencoded.append("name", this.state.request.client.name);
    urlencoded.append("email", this.state.request.client.email);
    urlencoded.append("phone", this.state.request.client.phone);
    urlencoded.append("default", this.state.request.client.default);

    const requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: urlencoded,
    };

    fetch("https://api.hachikopets.com/client", requestOptions)
      .then(res => {
        if (res.status === 200) {
          res.text().then(res => {
            const resJSON = JSON.parse(res);

            database.ref('gateways/clients/' + this.props.user.uid)
              .set(resJSON)
              .then(() => {
                this.setState({
                  ...this.state,
                  response: {
                    ...this.state.response,
                    client: resJSON,
                  },
                });
              })
              .then(() => {
                ToastsStore.info("Procesando datos de usuario");
                this.handlePayment(token_card, resJSON.id);
              }).catch(err => {
              ToastsStore.error(err.message);
            });
          });
        }
      }).catch(err => {
        ToastsStore.error(err.message);
    });
  };

  handlePayment = (token_card, customer_id) => {
    const myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer U2FsdGVkX1+FgvJxKZc0mT5BeGDhG2joQvtxxhobgC0=");
    myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

    const tmpName = this.state.request.client.name.split(" ");
    const urlencoded = new URLSearchParams();
    urlencoded.append("token_card", token_card);
    urlencoded.append("customer_id", customer_id);
    urlencoded.append("doc_type", this.state.request.payment.doc_type);
    urlencoded.append("doc_number", this.state.request.payment.doc_number);
    urlencoded.append("name", tmpName[0]);
    urlencoded.append("last_name", tmpName[1]);
    urlencoded.append("email", this.state.request.client.email);
    urlencoded.append("bill", "Hachiko Pets: COMPRA");
    urlencoded.append("description", this.state.request.payment.description);
    urlencoded.append("value", this.state.request.payment.value);
    urlencoded.append("tax", "0");
    urlencoded.append("tax_base", "0");
    urlencoded.append("currency", "COP");
    urlencoded.append("dues", this.state.request.payment.dues);

    const requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: urlencoded,
    };

    fetch("https://api.hachikopets.com/card/payment", requestOptions)
      .then(res => {
        if (res.status === 200) {
          res.text().then(res => {
            const resJSON = JSON.parse(res);

            database.ref('gateways/payments')
              .child(this.props.uid)
              .push(resJSON)
              .then(() => {
                this.setState({
                  ...this.state,
                  response: {
                    ...this.state.response,
                    payment: resJSON,
                  },
                });
                ToastsStore.success("Pago efectuado con éxito");
                window.open("https://wwww.hachikopets.com/compraexitosa", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=600,height=600");
              }).catch(err => {
                ToastsStore.error(err.message);
                console.log(err);
              });

          });
        }
      }).catch(err => {
        ToastsStore.error(err.message);
    });
  };

  render() {
    return (
      <React.Fragment>
        <form className="needs-validation" noValidate onSubmit={this.handlePaymentCARD}>
          <div className="row">
            <div className="col-md-6 mb-3">
              <label>Tipo de documento</label>
              <select
                className="custom-select d-block w-100"
                name="doc_type"
                defaultValue={'CC'}
                onChange={this.handleChange}
                value={this.state.request.payment.doc_type}
                required
              >
                <option value={'CC'}>Cédula de ciudadanía</option>
                <option value={'CE'}>Cédula de extrangería</option>
                <option value={'DNI'}>Identificación extranjera</option>
                <option value={'PPN'}>Pasaporte</option>
                <option value={'NIT'}>NIT</option>
              </select>
            </div>
            <div className="col-md-6 mb-3">
              <label>Número de documento</label>
              <input
                type="number"
                className="form-control"
                onChange={this.handleChange}
                value={this.state.request.payment.doc_number}
                name="doc_number"
                required
              />
              {this.validator.message('doc_number', this.state.request.payment.doc_number, 'required')}
            </div>
            <div className="col-md-6 mb-3">
              <label>Nombre del propietario</label>
              <input
                type="text"
                className="form-control"
                onChange={this.handleChangeCLIENT}
                value={this.state.request.client.name}
                name="name"
                required
              />
              {this.validator.message('name', this.state.request.client.name, 'required')}
            </div>
            <div className="col-md-6 mb-3">
              <label>Número de teléfono</label>
              <input
                type="phone"
                name="phone"
                className="form-control"
                placeholder="(300) 000-0000"
                onChange={this.handleChangeCLIENT}
                value={this.state.request.client.phone}
                required
              />
              {this.validator.message('phone', this.state.request.client.phone, 'required')}
            </div>
            <div className="col-md-12 mb-3">
              <label>Correo electrónico</label>
              <input
                type="email"
                className="form-control"
                onChange={this.handleChangeCLIENT}
                value={this.state.request.client.email}
                name="email"
                required
              />
              {this.validator.message('email', this.state.request.client.email, 'required')}
            </div>
          </div>
          <div className="row">
            <div className="col-md-6 mb-3">
              <label>Número de la tarjeta</label>
              <NumberFormat
                name="number"
                className="form-control"
                format={"#### #### #### ####"}
                placeholder="**** **** **** ****"
                onChange={this.handleChangeCARD}
                value={this.state.request.card.number}
                required
              />
              {this.validator.message('number', this.state.request.card.number, 'required')}
            </div>
            <div className="col-md-2 mb-3">
              <label>Año</label>
              <NumberFormat
                name="exp_year"
                className="form-control"
                onChange={this.handleChangeCARD}
                value={this.state.request.card.exp_year}
                format={'####'}
                placeholder="****"
                required
              />
              {this.validator.message('exp_year', this.state.request.card.exp_year, 'required')}
            </div>
            <div className="col-md-2 mb-3">
              <label>Mes</label>
              <NumberFormat
                name="exp_month"
                className="form-control"
                onChange={this.handleChangeCARD}
                value={this.state.request.card.exp_month}
                format={'##'}
                placeholder="**"
                required
              />
              {this.validator.message('exp_month', this.state.request.card.exp_month, 'required')}
            </div>
            <div className="col-md-2 mb-3">
              <label>CVC</label>
              <NumberFormat
                name="cvc"
                className="form-control"
                onChange={this.handleChangeCARD}
                value={this.state.request.card.cvc}
                format={'###'}
                placeholder="***"
                required
              />
              {this.validator.message('cvc', this.state.request.card.cvc, 'required')}
            </div>
            <div className="col-md-10 mb-3">
              <label>Descripción</label>
              <input
                type={'text'}
                name="description"
                className="form-control"
                onChange={this.handleChange}
                value={this.state.request.payment.description}
                required
              />
              {this.validator.message('description', this.state.request.payment.description, 'required')}
            </div>
            <div className="col-md-2 mb-3">
              <label>Cuotas</label>
              <input
                type={'number'}
                name="dues"
                className="form-control"
                onChange={this.handleChange}
                value={this.state.request.payment.dues}
                required
              />
              {this.validator.message('dues', this.state.request.payment.dues, 'required')}
            </div>
          </div>
          <ToastsContainer position={ToastsContainerPosition.BOTTOM_CENTER} store={ToastsStore}/>
          <div className="row">
            <div className="col-12">
              <input
                className={"btn btn-primary btn-lg btn-block"}
                value={'Procesar pedido'}
                type={"submit"}
              />
            </div>
          </div>
        </form>
      </React.Fragment>
    );
  }
  componentDidMount() {
    this.fetchUserInfo();
  }
  componentWillUnmount() {
    this.fetchUserInfo();
  }
}

const mapStateToProps = state => ({
  uid: state.user.uid,
  user: state.user,
  delivery: state.delivery,
  location: state.location,
  timezone: state.timezone,
  totalCart: state.totalCart,
  products: state.products,
  paymentGateway: state.paymentGateway,
});

const mapDispatchToProps = dispatch => ({
  handleCheckout(checkout){
    dispatch({
      type: 'SET_CHECKOUT',
      checkout
    })
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(CARDCheckout);
