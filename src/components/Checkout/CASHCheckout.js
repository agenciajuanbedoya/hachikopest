import React, {Component} from "react";
import {Link} from 'react-router-dom';
import {connect} from "react-redux";
import uuid from "react-uuid";
import {database} from "../../core/firebase";

const uid = uuid();

class CASHCheckout extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleChangeDelivery = e => {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value,
    });
  };

  pushOrder = () => {
    database.ref('orders/' + this.props.user + '/' + uid)
      .set({
        uid: uid,
        type: 'CASH',
        date: new Date(),
        user: this.props.user,
        delivery: this.props.delivery,
        location: this.props.location,
        timezone: this.props.timezone,
        products: this.props.products,
        totalCart: this.props.totalCart,
        checkout: this.props,
        paymentGateway: this.props.paymentGateway,
      }).then(() => {
        window.open("https://wwww.hachikopets.com/compraexitosa", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,width=600,height=600");
      }).catch(err => {
        console.log(err);
      })
  };

  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-md-6 mb-3">
            <label>¿Requiere de vuelta?</label>
            <input
              type="text"
              className="form-control"
              onChange={this.handleChangeDelivery}
              value={this.state.request}
              name="request"
              required
            />
          </div>
          <div className="col-md-6 mb-3">
            <label>Monto</label>
            <input
              type="number"
              className="form-control"
              name="value"
              onChange={this.handleChangeDelivery}
              value={this.state.value}
              required
            />
          </div>
          <div className="col-md-12 mb-3">
            <label>¿Requiere datáfono?</label>
            <select
              className="custom-select d-block w-100"
              value={this.state.dataphone}
              defaultValue={''}
              name="dataphone"
              onChange={this.handleChangeDelivery}
              required
            >
              <option value={''} disabled>Elija una opción</option>
              <option value={'Si'}>Sí, pagaré con Datáfono</option>
              <option value={'No'}>No, pagaré en efectivo</option>
            </select>
          </div>
        </div>

        <div className="row">
          <div className="col-12 mb-3">
            <Link
              className={"btn btn-primary btn-lg btn-block"}
              to={'#'}
              onClick={() => this.pushOrder()}
              type={"submit"}
            >
              Solicitar entrega
            </Link>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  delivery: state.delivery,
  location: state.location,
  timezone: state.timezone,
  totalCart: state.totalCart,
  products: state.products,
  paymentGateway: state.paymentGateway,

});

export default connect(mapStateToProps)(CASHCheckout);
