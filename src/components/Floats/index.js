import React from "react";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faShoppingCart, faUser} from "@fortawesome/free-solid-svg-icons";
import {faWhatsapp} from "@fortawesome/free-brands-svg-icons";
import './styles/index.css';

const Floats = ({products}) => {
  if (products.length !== 0) {
    return (
      <div className={'Float__root'}>
        <Link to={'/dashboard'}>
          <div className={'Float__btn animated bounceInLeft Float__btn-profile'}>
            <FontAwesomeIcon className={'Float__btn-content'} icon={faUser} style={{color: '#FFFFFF'}}/>
          </div>
        </Link>

        <Link to={'/cart'}>
          <div className={'Float__btn-cart animated bounceInLeft Float__btn-card'}>
            <div className={'Float__btn-cart_number'}>{products.length}</div>
            <FontAwesomeIcon className={'Float__btn-content'} icon={faShoppingCart} style={{color: '#FFFFFF'}}/>
          </div>
        </Link>

        <a href="https://wa.me/+573194775967" target="_blank" rel="noopener noreferrer">
          <div className={'Float__btn animated bounceInLeft Float__btn-profile'}>
            <FontAwesomeIcon className={'Float__btn-content'} icon={faWhatsapp} style={{color: '#FFFFFF'}}/>
          </div>
        </a>
      </div>
    );
  } else {
    return (
      <div className={'Float__root'}>
        <Link to={'/dashboard'}>
          <div className={'Float__btn animated bounceInLeft Float__btn-profile'} style={{backgroundColor: '#004CC4'}}>
            <FontAwesomeIcon className={'Float__btn-content'} icon={faUser} style={{color: '#FFFFFF'}}/>
          </div>
        </Link>

        <Link to={'/cart'}>
          <div className={'Float__btn animated bounceInLeft Float__btn-card'} style={{backgroundColor: '#004CC4'}}>
            <FontAwesomeIcon className={'Float__btn-content'} icon={faShoppingCart} style={{color: '#FFFFFF'}}/>
          </div>
        </Link>

        <a href="https://wa.me/+573194775967" target="_blank" rel="noopener noreferrer">
          <div className={'Float__btn animated bounceInLeft Float__btn-profile'}>
            <FontAwesomeIcon className={'Float__btn-content'} icon={faWhatsapp} style={{color: '#FFFFFF'}}/>
          </div>
        </a>
      </div>
    );
  }
};

const mapStateToProps = state => ({
  products: state.products,
});


const mapDispatchToProps = dispatch => ({
  handleCity(city){
    dispatch({
      type: 'CHANGE_CITY',
      city
    })
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Floats);
