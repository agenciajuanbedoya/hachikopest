import React, {Component} from "react";
import './Styles/TitleSubtitle.css';

class TitleSubtitle extends Component {
    render() {
        return (
            <div className={'justify-content-center'}>
                <div className={'col-auto text-center'}>
                    <h6 className={'TitleSubtitle__subtitle'}>{this.props.subtitle}</h6>
                    <h4 className={'TitleSubtitle__title'}>{this.props.title}</h4>
                </div>
            </div>
        );
    }
}

export default TitleSubtitle;
