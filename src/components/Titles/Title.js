import React, {Component} from "react";

import './Styles/Title.css';

class Title extends Component {
    render() {
        return <h2 className={'text-center Title__text'}>{this.props.text}</h2>;
    }
}

export default Title;
