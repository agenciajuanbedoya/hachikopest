import React, {Component} from "react";
import {Link} from "react-router-dom";
import {auth} from "../../core/firebase";
import './styles/DashboardSideBar.css';
import {connect} from "react-redux";

class DashboardSideBar extends Component {
  signout = () => {
    auth.signOut().then(() => {
      this.props.handleUser(undefined)
      console.log('Te esperamos pronto');
    }).catch(error => {
      console.log(error);
    });
  };
  render() {
    return (
      <div className={'DashboardSideBar__main'}>
        <ul className="nav flex-column">
          <li className="DashboardSideBar__main-nav-item">
            <Link to={"/dashboard"}>Mi cuenta</Link>
          </li>
          <li className="DashboardSideBar__main-nav-item">
            <Link to={"/dashboard/pets"}>Mis peludos</Link>
          </li>
          <li className="DashboardSideBar__main-nav-item">
            <Link to={"/dashboard/orders"}>Mis pedidos</Link>
          </li>
          <li className="DashboardSideBar__main-nav-item">
            <Link to={"/dashboard/memberships"}>Mis membresías</Link>
          </li>
          <li className="DashboardSideBar__main-nav-item">
            <Link to={'/'} onClick={() => {this.signout()}}>
              Cerrar sesión
            </Link>
          </li>
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.products,
  location: state.location,
});

const mapDispatchToProps = dispatch => ({
  handleUser(user){
    dispatch({
      type: 'SET_USER',
      user
    })
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardSideBar);
