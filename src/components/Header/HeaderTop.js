import React, {Component} from "react";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faWhatsapp, faFacebookF, faInstagram} from "@fortawesome/free-brands-svg-icons";
import {connect} from "react-redux";
import './Styles/HeaderTop.css';

const TextVariable = ({location}) => {
    if (location === 'Medellin') {
        return (
          <h3 className={'Header-Top__text'} style={{color: '#FFFFFF'}}>
              Ahora disponible en Medellín, Sabaneta, Bello, Envigado e Itaguí
            <a href="https://wa.me/+573194775967" rel="noopener noreferrer">
              <FontAwesomeIcon className={'Header-Top__icon'} icon={faWhatsapp}/> (319) 477-5967
            </a>
            <a href="https://www.facebook.com/hachikopetslovers/" rel="noopener noreferrer">
              <FontAwesomeIcon className={'Header-Top__icon'} icon={faFacebookF}/>
            </a>
            <a href="https://www.instagram.com/hachiko_pets/" rel="noopener noreferrer">
              <FontAwesomeIcon className={'Header-Top__icon'} icon={faInstagram}/>
            </a>
          </h3>
        );
    } else if (location === 'Bucaramanga') {
        return (
          <h3 className={'Header-Top__text'} style={{color: '#FFFFFF'}}>
              Ahora disponible en Bucaramanga, Floridablanca, Piedecuestas y Girón
            <a href="https://wa.me/+573194775967" rel="noopener noreferrer">
              <FontAwesomeIcon className={'Header-Top__icon'} icon={faWhatsapp}/> (319) 477-5967
            </a>
            <a href="https://www.facebook.com/hachikopetslovers/" rel="noopener noreferrer">
              <FontAwesomeIcon className={'Header-Top__icon'} icon={faFacebookF}/>
            </a>
            <a href="https://www.instagram.com/hachiko_pets/" rel="noopener noreferrer">
              <FontAwesomeIcon className={'Header-Top__icon'} icon={faInstagram}/>
            </a>
          </h3>
        );
    } else {
        return (
          <h3 className={'Header-Top__text'} style={{color: '#FFFFFF'}}>
              Hachiko Pets ahora disponible en Bucaramanga y Medellín
            <a href="https://wa.me/+573194775967" rel="noopener noreferrer">
              <FontAwesomeIcon className={'Header-Top__icon'} icon={faWhatsapp}/> (319) 477-5967
            </a>
            <a href="https://www.facebook.com/hachikopetslovers/" rel="noopener noreferrer">
              <FontAwesomeIcon className={'Header-Top__icon'} icon={faFacebookF}/>
            </a>
            <a href="https://www.instagram.com/hachiko_pets/" rel="noopener noreferrer">
              <FontAwesomeIcon className={'Header-Top__icon'} icon={faInstagram}/>
            </a>
          </h3>
        );
    }
};

class HeaderTop extends Component {
  handleChange = e => {
    this.props.handleCity(e.target.value);
  };

  render() {
    return (
      <div
        className={'Header-Top__container animated fadeInDown fast'}
        style={{backgroundColor: this.props.backgroundColor}}>
        <div className={'container'}>
          <div className={'row justify-content-xl-between justify-content-lg-between justify-content-md-between justify-content-sm-center justify-content-center align-items-center'}>
            <div className="col-auto order-xl-1 order-lg-1 order-md-2 order-sm-2 order-2">
              <div className="select">
                <select
                  id="slct"
                  name="slct"
                  value={this.props.location}
                  onChange={this.handleChange}>
                  <option value={'Bogota'}>Bogotá</option>
                  <option value={'Bucaramanga'}>Bucaramanga</option>
                  <option value={'Medellin'}>Medellín</option>
                </select>
              </div>
            </div>
            <div className={'col-auto order-xl-2 order-lg-2 order-md-1 order-sm-1 order-1'}>
              <TextVariable location={this.props.location} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
    location: state.location,
    pet: state.pet,
});

const mapDispatchToProps = dispatch => ({
    handleCity(city){
        dispatch({
            type: 'CHANGE_CITY',
            city
        })
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(HeaderTop);
