import React, {Fragment} from "react";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBars} from "@fortawesome/free-solid-svg-icons";
import './Styles/HeaderMenu.css';

function HeaderMenu(props) {
  return (
    <Fragment>
      <nav>
        <ul>
          <li><Link to="/" style={{color: props.color}}>Inicio</Link></li>
          <li><Link to="/services" style={{color: props.color}}>Servicios</Link></li>
          <li><Link to="/products" style={{color: props.color}}>Productos</Link></li>
          <li><Link to="/coverage" style={{color: props.color}}>Cobertura</Link></li>
          <li><Link to="/offers" style={{color: props.color}}>Ofertas</Link></li>
          <li><Link to="#" onClick={() => window.location.href = 'https://www.hachikopets.com/blog'} style={{color: props.color}}>Blog</Link></li>
          <li><Link to="/contact" style={{color: props.color}}>Contacto</Link></li>
        </ul>
        <Link id="pull" to="#" onClick={() => {
          document.getElementById('mobile-menu').style.display = "block";
          window.onclick = function(event) {
            if (event.target === document.getElementById('mobile-menu')) {
              document.getElementById('mobile-menu').style.display = "none";
            }
          }
        }} style={{backgroundColor: props.color, fontWeight: 700}}
        >
          Menú principal <FontAwesomeIcon className={"menu-sized"} icon={faBars} />
        </Link>
      </nav>

      <div id="mobile-menu" className="mobile-menu">
        <div className="mobile-menu-content animated zoomIn faster">
          <div className="mobile-menu-body nav">
            <ul className={"mobile-menu-pages"}>
              <li><Link to="/" style={{color: props.color}}>Inicio</Link></li>
              <li><Link to="/services" style={{color: props.color}}>Servicios</Link></li>
              <li><Link to="/products" style={{color: props.color}}>Productos</Link></li>
              <li><Link to="/coverage" style={{color: props.color}}>Cobertura</Link></li>
              <li><Link to="/offers" style={{color: props.color}}>Ofertas</Link></li>
              <li><Link to="#" onClick={() => window.location.href = 'https://www.hachikopets.com/blog'} style={{color: props.color}}>Blog</Link></li>
              <li><Link to="/contact" style={{color: props.color}}>Contacto</Link></li>
            </ul>
            <Link
              to={"#"}
              style={{ color: props.color }}
              onClick={() => {
                document.getElementById('mobile-menu').style.display = "none";
                window.onclick = function(event) {
                  if (event.target === document.getElementsByClassName('mobile-menu')) {
                    document.getElementsByClassName('mobile-menu').style.display = "none";
                  }
                }
              }}
              className="close"
            >
              Cerrar
            </Link>
          </div>
        </div>
      </div>

    </Fragment>
  );
}

export default HeaderMenu;
