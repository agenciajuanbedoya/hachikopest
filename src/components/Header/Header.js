import React, {Component} from "react";
import {Link} from "react-router-dom";
import Floats from "../Floats";
import HeaderTop from "./HeaderTop";
import HeaderSearch from "./HeaderSearch";
import HeaderMenu from "./HeaderMenu";
import './Styles/Header.css';

class Header extends Component {
  render() {
    return (
      <header className={'Header__container'}>
        <Floats backgroundColor={this.props.colorBg} textColor={this.props.colorText} />
        <HeaderTop backgroundColor={this.props.colorBg} textColor={this.props.colorText} />
        <div className={'container'}>
          <div className={'row justify-content-center Header__logo animated zoomIn fast'}>
            <div className={'col-auto'}>
              <Link to={"/"}>
                <span style={{ color: this.props.colorBg }}>Hachiko Pets</span>
              </Link>
            </div>
          </div>
          <HeaderSearch backgroundColor={this.props.colorBg} textColor={this.props.colorText} />
          <div className={'row justify-content-center animated fadeInUp fast'}>
            <div className={'col-auto'}>
              <HeaderMenu color={this.props.colorBg} />
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
