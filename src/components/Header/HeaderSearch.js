import React, {Component} from "react";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSearch} from "@fortawesome/free-solid-svg-icons";
import {Link, Redirect} from "react-router-dom";
import './Styles/HeaderSearch.css';

class HeaderSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchterms: '',
      redirection: false,
    };
  };

  handleChange = e => {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value,
    });
  };

  handleSearch = e => {
    e.preventDefault();
    this.setState({
      ...this.state,
      redirection: true,
    });
  };

  render() {
    if (this.state.redirection) {
      return <Redirect to={"/search/" + this.state.searchterms} />;
    } else {
      return (
        <div className={'row justify-content-center animated fadeInUp fast'}>
          <div className={'col-sm-12 col-md-6'}>
            <form className={'HeaderSearch__content'} onSubmit={this.handleSearch}>
              <div className={'input-group mb-3'}>
                <input
                  type={'text'}
                  name={'searchterms'}
                  value={this.state.searchterms}
                  onChange={this.handleChange}
                  placeholder={'Estoy buscando...'}
                  className={'HeaderSearch__input form-control'}
                />
                <div className={'HeaderSearch__buttom input-group-append'} style={{ backgroundColor: this.props.backgroundColor }}>
                  <Link to={`/search/${this.state.searchterms}`}>
                    <button className={'btn'} type={'button'}>
                      <FontAwesomeIcon icon={faSearch} className={'HeaderSearch__icon'} />
                    </button>
                  </Link>
                </div>
              </div>
            </form>
          </div>
        </div>
      );
    }
  }
}

export default HeaderSearch;
