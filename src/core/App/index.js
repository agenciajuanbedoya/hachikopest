import React, {Component} from "react";
import {Provider} from "react-redux";
import Routes from "../Routes";
import Reducers from '../Reducers'

import './styles/index.css';

class App extends Component {
  render() {
    return (
      <Provider store={Reducers}>
        <Routes />
      </Provider>
    );
  }
}

export default App;
