import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import {LocationApi, TimeApi} from "../Api";
import {auth} from "../firebase";
import {connect} from "react-redux";
import Home from "../../pages/Home";
import Product from "../../pages/Product";
import Products from "../../pages/Products";
import Category from "../../pages/Category";
import Welcome from "../../pages/Welcome";
import Services from "../../pages/Services";
import Coverage from "../../pages/Coverage";
import Offers from "../../pages/Offers";
import Contact from "../../pages/Contact";
import ShoppingCart from "../../pages/ShoppingCart";
import NotFound from "../../pages/NotFound";
import Login from "../../pages/Login";
import Dashboard from "../../pages/Dashboard";
import Search from "../../pages/Search";
import PetsDashboard from "../../pages/Dashboard/PetsDashboard";
import OrdersDashboard from "../../pages/Dashboard/OrdersDashboard";
import MembershipsDashboard from "../../pages/Dashboard/MembershipsDashboard";
import ForgotPassword from "../../pages/Login/ForgotPassword";
import Register from "../../pages/Login/Register";
import VeterinarioEnCasaService from "../../pages/Service/VeterinarioEnCasaService";
import GuarderiaAnimalService from "../../pages/Service/GuarderiaAnimalService";
import BanoADomicilioService from "../../pages/Service/BanoADomicilioService";
import ServiciosFunerariosService from "../../pages/Service/ServiciosFunerariosService";
import EstudioFotograficoService from "../../pages/Service/EstudioFotograficoService";
import BuscadorDeParejaService from "../../pages/Service/BuscadorDeParejaService";
import DenunciaMaltratoService from "../../pages/Service/DenunciaMaltratoService";
import Cookies from "js-cookie";
import Legal from "../../pages/Legal";

class Routes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // user: false,
      login: false,
      locationApi: {},
      timeApi: {},
      loading: true,
    }
  }

  verify = () => {
    auth.onAuthStateChanged(user => {
      if (user) {
        this.props.handleUser(user);
      }
    }, error => {
      return console.log(error)
    }, r => {
      return console.log(r)
    });
  };

  autolocation = async () => {
    const res = await fetch(LocationApi);
    const data = await res.json();
    if (Cookies.get('hachikopets_loc') === undefined) {
      if (data.city === 'Medellín' || data.region === 'Antioquia') {
        this.props.handleCity('Medellin');
        Cookies.set('hachikopets_loc', 'Medellin', { expires: 7, path: '/' });

      } else if (data.city === 'Bogota' || data.region === 'Cundinamarca') {
        this.props.handleCity('Bogota');
        Cookies.set('hachikopets_loc', 'Bogota', { expires: 7, path: '/' });

      } else if (data.city === 'Bucaramanga' || data.region === 'Santander') {
        this.props.handleCity('Bucaramanga');
        Cookies.set('hachikopets_loc', 'Bucaramanga', { expires: 7, path: '/' });

      } else {
        this.props.handleCity('Bogota');
        Cookies.set('hachikopets_loc', 'Bogota', { expires: 7, path: '/' });
      }
    } else {
      this.props.handleCity(Cookies.get('hachikopets_loc'));
    }
  };

  autotime = async () => {
    const res = await fetch(TimeApi);
    const data = await res.json();
    this.props.handleTime(data)
  };

  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/">
            {this.props.pet ? <Home /> : <Welcome />}
          </Route>
          <Route exact path="/services">
            {this.props.pet ? <Services /> : <Welcome />}
          </Route>
          <Route exact path="/products">
            {this.props.pet ? <Products /> : <Welcome />}
          </Route>
          <Route exact path="/coverage">
            {this.props.pet ? <Coverage /> : <Welcome />}
          </Route>
          <Route exact path="/offers">
            {this.props.pet ? <Offers /> : <Welcome />}
          </Route>
          <Route exact path="/contact">
            {this.props.pet ? <Contact /> : <Welcome />}
          </Route>
          <Route exact path="/dashboard">
            {this.props.user ? <Dashboard /> : <Login />}
          </Route>
          <Route exact path="/dashboard/pets">
            {this.props.user ? <PetsDashboard /> : <Login />}
          </Route>
          <Route exact path="/dashboard/orders">
            {this.props.user ? <OrdersDashboard /> : <Login />}
          </Route>
          <Route exact path="/dashboard/memberships">
            {this.props.user ? <MembershipsDashboard /> : <Login />}
          </Route>
          <Route exact path="/forgot-password/">
            <ForgotPassword />
          </Route>
          <Route exact path="/register/">
            <Register />
          </Route>

          { this.props.pet ?
            <Route path="/product/:id" component={Product} />
            :
            <Welcome />
          }
          { this.props.pet ?
            <Route path="/search/:attr" component={Search} />
            :
            <Welcome />
          }
          { this.props.pet ?
            <Route path="/category/:attr" component={Category} />
            :
            <Welcome />
          }

          <Route path="/cart" component={ShoppingCart} />
          <Route path="/servicio/veterinario-en-casa" component={VeterinarioEnCasaService} />
          <Route path="/servicio/guarderia-animal" component={GuarderiaAnimalService} />
          <Route path="/servicio/bano-a-domicilio" component={BanoADomicilioService} />
          <Route path="/servicio/servicios-funerarios" component={ServiciosFunerariosService} />
          <Route path="/servicio/estudio-fotografico" component={EstudioFotograficoService} />
          <Route path="/servicio/buscador-de-pareja" component={BuscadorDeParejaService} />
          <Route path="/servicio/denunciar-maltrato" component={DenunciaMaltratoService} />
          <Route path="/legal" component={Legal} />
          <Route component={NotFound} />
        </Switch>
      </BrowserRouter>
    );
  }

  componentDidMount() {
    this.autolocation();
    this.autotime();
    this.verify();
  }

  componentWillUnmount() {
    this.autolocation();
    this.autotime();
    this.verify();
  }
}

const mapStateToProps = state => ({
  location: state.location,
  user: state.user,
  pet: state.pet,
});

const mapDispatchToProps = dispatch => ({
  handleCity(city){
    dispatch({
      type: 'CHANGE_CITY',
      city
    })
  },
  handleTime(time){
    dispatch({
      type: 'CHANGE_TIME',
      time
    })
  },
  handleUser(user){
    dispatch({
      type: 'SET_USER',
      user
    })
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Routes);
