import React, {Component} from "react";
import {auth} from "../../core/firebase";

class Verify extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: ''
    }
  }

  verify = () => {
    auth.onAuthStateChanged((user) => {
      if (user) {
        return console.log('Logueado: ' + user.uid);
      } else {
        return console.log('Visitante');
      }
    }, error => {
      return console.log(error)
    }, r => {
      return console.log(r)
    });
  };

  render() {
    return (
      <div>
        {this.state.user.uid}
      </div>
    );
  }

  componentDidMount() {
    this.verify()
  }

  componentWillUnmount() {
    this.verify()
  }
}

export default Verify;
