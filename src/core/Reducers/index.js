import {createStore} from "redux";

const initialState = {
  pet: undefined,
  city: [
    {
      id: 1,
      title: 'Bogotá',
      image: '#',
      slug: 'Bogota'
    },
    {
      id: 2,
      title: 'Bucaramanga',
      image: '#',
      slug: 'Bucaramanga'
    },
    {
      id: 3,
      title: 'Medellín',
      image: '#',
      slug: 'Medellin'
    }
  ],
  products: [],
  totalCart: 0,
  location: undefined,
  time: {},
  user: false,
  date: '',
  delivery: undefined,
  timezone: {
    title: 'Titulo',
    price: 0,
  },
  checkout: {},
  paymentGateway: 'CARD',
  colorPrimary: '#004CC4',
  hintPrimary: '#FFFFFF',
  colorSecundary: '#c95937',
  hintSecundary: '#FFFFFF',
};

const Reducers = (state = initialState, action) => {
  if (action.type === "CHANGE_CITY") {
    return {
      ...state,
      location: action.city
    }
  } else if (action.type === "CHANGE_PET") {
    return {
      ...state,
      pet: action.pet
    }
  } else if (action.type === "ADD_PRODUCT") {
    const ProductItem = action.product;
    const ShoppingCart = state.products;
    let TotalCart = state.totalCart;
    const VerifyShoppingCart = ShoppingCart.filter(item => item.id === ProductItem.id);
    if (VerifyShoppingCart.length !== 0) {
      // Existe
      const AfterProductItem = VerifyShoppingCart[0];
      const IndexProduct = ShoppingCart.indexOf(AfterProductItem);

      const AfterQuantity = AfterProductItem.quantity;
      const AfterPrice = AfterProductItem.quantity * AfterProductItem.price;
      TotalCart = TotalCart - AfterPrice;

      const BeforeQuantity = ProductItem.quantity + AfterQuantity;
      const BeforePrice = BeforeQuantity * ProductItem.price;
      TotalCart = TotalCart + BeforePrice;

      ShoppingCart[IndexProduct].quantity = BeforeQuantity;
      return {
        ...state,
        totalCart: TotalCart,
        products: ShoppingCart,
      }

    } else {
      // No existe
      if (ProductItem.quantity !== 1) {
        const varPrice = ProductItem.price * ProductItem.quantity;
        return {
          ...state,
          totalCart: state.totalCart + varPrice,
          products: [
            ...state.products,
            ProductItem
          ]
        }
      } else {
        return {
          ...state,
          totalCart: state.totalCart + ProductItem.price,
          products: [
            ...state.products,
            ProductItem
          ]
        }
      }
    }
  } else if (action.type === "REMOVE_PRODUCT") {
    const ShoppingCart = state.products;
    const RemoveProduct = ShoppingCart[action.product];
    const UpdateShoppingCart = ShoppingCart.filter(item => item.id !== RemoveProduct.id);
    const RemovePrice = RemoveProduct.quantity * RemoveProduct.price;
    return {
      ...state,
      products: UpdateShoppingCart,
      totalCart: state.totalCart - RemovePrice,
    }
  } else if (action.type === "CHANGE_TIME") {
    return {
      ...state,
      time: action.datetime
    }
  } else if (action.type === "SET_PAYMENT_GATEWAY") {
    return {
      ...state,
      paymentGateway: action.paymentGateway
    }
  } else if (action.type === "SET_USER") {
    return {
      ...state,
      user: action.user
    }
  } else if (action.type === "SET_DELIVERY") {
    return {
      ...state,
      delivery: action.delivery
    }
  } else if (action.type === "RESET_TIMEZONE") {
    return {
      ...state,
      timezone: {
        title: 'Titulo',
        price: 0,
      },
    }
  } else if (action.type === "SET_DATE_SELECT") {
    return {
      ...state,
      date: action.date.toString()
    }
  } else if (action.type === "SET_CHECKOUT") {
    return {
      ...state,
      checkout: action.checkout,
    }
  } else if (action.type === "RESET_DELIVERY") {
    return {
      ...state,
      delivery: action.status,
    }
  } else if (action.type === "ADD_QUANTITY_CART") {
    const UnitPrice = state.products[action.quantity].price;
    const ShoppingCart = state.products;
    const UpdateProduct = ShoppingCart[action.quantity];
    UpdateProduct.quantity = UpdateProduct.quantity + 1;
    ShoppingCart[action.quantity] = UpdateProduct;
    const TotalPrice = state.totalCart + UnitPrice;
    return {
      ...state,
      products: ShoppingCart,
      totalCart: TotalPrice,
    }
  } else if (action.type === "MIN_QUANTITY_CART") {
    const UnitPrice = state.products[action.quantity].price;
    const ShoppingCart = state.products;
    const UpdateProduct = ShoppingCart[action.quantity];

    if (UpdateProduct.quantity > 1) {
      UpdateProduct.quantity = UpdateProduct.quantity - 1;
      ShoppingCart[action.quantity] = UpdateProduct;
      const TotalPrice = state.totalCart - UnitPrice;

      return {
        ...state,
        products: ShoppingCart,
        totalCart: TotalPrice,
      }
    }
  } else if (action.type === "SET_TIME_ZONE") {
    const afterTotal = state.totalCart - state.timezone.price;
    return {
      ...state,
      timezone: action.timezone,
      totalCart: afterTotal + action.timezone.price,
    }
  }

  return state
};

export default createStore(Reducers);
