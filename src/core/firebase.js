import * as firebase from "firebase/app";

import "firebase/auth";
import "firebase/storage";
import "firebase/database";
import "firebase/firestore";
import "firebase/analytics";
import "firebase/performance";

firebase.initializeApp({
  apiKey: "AIzaSyC2WZDJOLECVSodz9OewMI51Q9aNF15cGA",
  authDomain: "hachikopets-co.firebaseapp.com",
  databaseURL: "https://hachikopets-co.firebaseio.com",
  projectId: "hachikopets-co",
  storageBucket: "hachikopets-co.appspot.com",
  messagingSenderId: "748620235800",
  appId: "1:748620235800:web:7bb3b2eea1bd7833402f6d",
  measurementId: "G-S29PMYC9VJ"
});

export default firebase;
export const analytics = firebase.analytics();
export const auth = firebase.auth();
export const firestore = firebase.firestore();
export const database = firebase.database();
export const storage = firebase.storage();
export const performance = firebase.performance();
