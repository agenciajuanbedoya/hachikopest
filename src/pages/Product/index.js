import React, { Component } from "react";
import NumericInput from "react-numeric-input";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faShoppingCart} from "@fortawesome/free-solid-svg-icons";
import {connect} from "react-redux";
import toaster from "toasted-notes";
import Cookies from "js-cookie";
import BlueLayout from "../../layout/BlueLayout";
import API from "../../core/Api";
import './styles/index.css';
import 'toasted-notes/src/styles.css';

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      error: false,
      product: {},
      variations: [],
    };
  }

  formatText = item => {
    let filterList = ['[wpsm_list type="arrow"]', '[/wpsm_list]', '<h1>', '</h1>', 'medellin', 'bogota', 'bucaramanga']; // A reemplazar
    let elementList = ['', '', '<h2>', '</h2>', 'Medellín', 'Bogotá', 'Bucaramanga']; // Por lo que se va a reemplazar
    let formated = item; // Elementos formateados
    let i=0; // Contador

    // Función
    while (i<filterList.length) {
      // formated = formated.replace(filterList[i], "");
      formated = formated.split(filterList[i]).join(elementList[i]);
      i++
    }

    // Retorno
    return formated;
  };

  fetchData = async (id) => {
    const res = await fetch(API + `/product?location=${Cookies.get('hachikopets_loc').toLowerCase()}&id=${id}`);
    const data = await res.json();
    this.setState({
      ...this.state,
      product: data,
      loading: false,
    });
  };

  fetchVariation = async () => {
    const res = await fetch(API + `/product/var?location=${Cookies.get('hachikopets_loc').toLowerCase()}&sku=${this.state.product.sku}`);
    const data = await res.json();
    if (data.products !== null) {
      this.setState({
        ...this.state,
        product: {
          ...this.state.product,
          id: data.docs[0].id,
          _id: data.docs[0]._id,
          tag: data.docs[0].tag,
          sku: data.docs[0].sku,
          type: data.docs[0].type,
          title: data.docs[0].title,
          price: data.docs[0].price,
          quantity: data.docs[0].quantity,
        },
        variations: data.docs,
        loading: false,
      });
    }
  };

  setVariation = variation => {
    this.setState({
      ...this.state,
      product: {
        ...this.state.product,
        id: variation.id,
        _id: variation._id,
        tag: variation.tag,
        sku: variation.sku,
        type: variation.type,
        price: variation.price,
        title: variation.title,
        quantity: variation.quantity,
      },
    });
  };

  setQuantity = (e) => {
    this.setState({
      ...this.state,
      product: {
        ...this.state.product,
        quantity: e,
      },
    });
  };

  varTitle = title => {
    const tmp = title.split(' - ');
    return tmp[1];
  };

  formatPeso = new Intl.NumberFormat('es-CO', {
    style: 'currency',
    currency: 'COP',
    minimumFractionDigits: 0
  });

  render() {
    if (this.state.loading) {
      return (
        <div className={'loading'}>
          <div className="loader">Loading...</div>
        </div>
      )
    } else {
      const addProduct = () => {
        this.props.handleProducts(this.state.product);
        toaster.notify(({ onClose }) => (
          <div className={'row Toast__container'}>
            <img className={'col-4 Toast__container-image'} src={this.state.product.images} alt={this.state.product.title} />
            <div className="col-8 text-left">
              <span className={'Toast__container-title'}>Añadido al carrito</span>
              <p>
                {this.state.product.title}
              </p>
              {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
              <a className={'Toast__container-btn'} onClick={onClose}>Seguir comprando</a>
            </div>
          </div>
        ));
      };

      return (
        <BlueLayout>
          <div className={"container ProductOptions__container"}>
            <div className={"row align-items-center"}>
              <div className={"col-xl-4 col-lg-4 col-md-4 col-sm-12"}>
                <div className={"card text-center"}>
                  <img
                    className={"card-img ProductOptions__image"}
                    src={this.state.product.images}
                    alt={this.state.product.title}
                  />
                  <div className={"card-body"}>
                    <h4 className={"card-text"}>Disponible desde</h4>
                    <button
                      type="button"
                      className="ProductOptions__btn-add-card"
                      onClick={() => { addProduct() }}
                    >
                      {this.formatPeso.format(this.state.product.price)}
                      <FontAwesomeIcon
                        className={'Header-Top__icon'}
                        icon={faShoppingCart}
                      />
                    </button>
                  </div>
                </div>
              </div>
              <div className={"col-xl-8 col-lg-8 col-md-8 col-sm-12"}>
                <div className={"ProductOptions__description"}>
                  <h1 className={"ProductOptions__title"}>{this.state.product.title}</h1>
                  <span className={"ProductOptions__description-metas justify-content-sm-center"}>Categoría: <strong>{this.state.product.category}</strong> | Marca: <b>{this.state.product.tag}</b> | Envios: <b>{this.formatText(Cookies.get('hachikopets_loc').toLowerCase())}</b></span>
                  <p className={"ProductOptions__description-content"} dangerouslySetInnerHTML={{__html: this.formatText(this.state.product.description_short)}}/>
                </div>
                <div className="btn-group btn-block" role="group">
                  {this.state.variations.map((item, index) => (
                    <React.Fragment key={index}>
                      {this.state.product.title === item.title ? (
                        <button type="button" className="ProductOptions__btn-size-active" onClick={() => this.setVariation(item)}>{this.varTitle(item.title)}</button>
                      ) : (
                        <button type="button" className="ProductOptions__btn-size" onClick={() => this.setVariation(item)}>{this.varTitle(item.title)}</button>
                      )}
                    </React.Fragment>
                  ))}
                </div>
                <div className={"row justify-content-center"}>
                  <div className={"col-auto"}>
                    <div className={"ProductOptions__count-items"}>
                      <div className={"ProductOptions__count-items-title"}>
                      <span className={"text-uppercase"}>
                        Cantidad
                      </span>
                      </div>
                      <div className={"ProductOptions__count-items-counter"}>
                        <NumericInput
                          className="form-control"
                          value={this.state.product.quantity}
                          min={1}
                          max={9999}
                          step={1}
                          precision={0}
                          size={5}
                          mobile
                          pattern="[0-9].[0-9][0-9]"
                          inputMode="numeric"
                          strict
                          onChange={(e) => this.setQuantity(e)}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className={"row ProductOptions__container-content"}>
              <div className={"col-12"}>
                <div className="card">
                  <div className="card-header">
                    <ul className="nav nav-tabs card-header-tabs">
                      <li className="nav-item">
                        <Link className="nav-link active" to="#">Descripción</Link>
                      </li>
                      <li className="nav-item">
                        <Link className="nav-link disabled" to="#" tabIndex="-1" aria-disabled="true">Valoraciones</Link>
                      </li>
                    </ul>
                  </div>
                  <div className="card-body">
                    <p className="card-text" dangerouslySetInnerHTML={{__html: this.formatText(this.state.product.description)}}/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </BlueLayout>
      );
    }
  }

  componentDidMount() {
    let { id } = this.props.match.params;
    this.fetchData(id)
      .then(r => {
        this.setState({ loading: false });
        window.scrollTo(0, 0);
        if (this.state.product.type === 'variable') {
          this.fetchVariation(id);
        }
      })
      .catch(r => {
        this.setState({
          loading: false,
          error: true,
        })
        }
      );
    console.clear();
  }

  componentWillUnmount() {
    this.fetchData();
  }
}

const mapStateToProps = state => ({
  products: state.products,
  location: state.location,
});

const mapDispatchToProps = dispatch => ({
  handleProducts(product){
    dispatch({
      type: 'ADD_PRODUCT',
      product
    })
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Product);
