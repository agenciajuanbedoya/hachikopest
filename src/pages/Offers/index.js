import React from "react";
import {connect} from "react-redux";
import Logo from '../../images/icons/logo-banner.png';
import BlueLayout from "../../layout/BlueLayout";
import './styles/index.css';

const Offerts = ({city, handleCity}) => {
  return(
    <BlueLayout>
      <div className={'WelcomePage__container container-fluid'}>
        <div className={'row'}>
          <div className={'col-auto'}>
            <img className={'WelcomePage__logo'} src={Logo} alt="Hachiko Pets"/>
          </div>
        </div>
        <div className={'row'}>
          <div className={'col-auto'}>
            <h1 className={'WelcomePage__title'}>Sin ofertas disponibles aún</h1>
          </div>
        </div>
      </div>
    </BlueLayout>
  )
};


const mapStateToProps = state => ({
  city: state.city
});

const mapDispatchToProps = dispatch => ({
  handleCity(city){
    dispatch({
      type: 'CHANGE_CITY',
      city
    })
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Offerts)
