import React, {Component} from "react"
import {connect} from "react-redux";
import Client from "../../components/Client";
import ShoppingOrders from "../../components/Shopping";
import DELIVERYCheckout from "../../components/Checkout/DELIVERYCheckout";
import BlueLayout from "../../layout/BlueLayout";
import "./styles/ShoppingCart.css";

class ShoppingCart extends Component {
  render() {
    return (
      <BlueLayout>
        <div className="container" style={{marginBottom: '40px', marginTop: '30px'}}>
          <div className="row">
            <div className="col-md-6 order-md-2 mb-4 ShoppingCart__Step">
                {this.props.user !== null ? (
                  <Client />
                ) : (
                  <DELIVERYCheckout />
                )}
            </div>
            <div className="col-md-6 order-md-1">
              <ShoppingOrders />
            </div>
          </div>
        </div>
      </BlueLayout>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

export default connect(mapStateToProps)(ShoppingCart);
