import React, {Component} from "react";
import {Link} from "react-router-dom";
import {auth} from "../../core/firebase";
import LoginImage from '../../images/login.png';
import BlueLayout from "../../layout/BlueLayout";
import SimpleReactValidator from "simple-react-validator";
import {ToastsStore} from "react-toasts";
import './styles/Login.css';

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
    };
    this.validator = new SimpleReactValidator({
      element: message => <div style={{color: 'red'}}>{message}</div>,
      messages: {
        default: 'Ingrese su correo electrónico',
      }, autoForceUpdate: this});
  }
  handleChange = e => {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value,
    });
  };
  handleForgotPassword = e => {
    e.preventDefault();
    if (this.validator.allValid()) {
      ToastsStore.info("Procesando información");
      auth.sendPasswordResetEmail(this.state.email)
        .then(() => {
          ToastsStore.success("Verifique su correo electrónico");
        }).catch(err => {
          ToastsStore.error(err.message);
      });
    } else {
      this.validator.showMessages();
      ToastsStore.error("Ingrese su correo electrónico");
      this.forceUpdate();
    }
  };
  render() {
    return (
      <BlueLayout>
        <div className={'container'} style={{ marginBottom: '20px' }}>
          <div className={'row justify-content-center align-items-center'} style={{marginTop: '40px'}}>
            <div className={'col-xl-4 col-lg-4 col-md-12 col-sm12 col-12 Login__main'}>
              <form noValidate onSubmit={this.handleForgotPassword}>
                <div className="row">
                  <div className="col-12">
                    <h4 style={{ marginBottom: '30px', fontWeight: 'bold'}}>Recuperar contraseña</h4>
                  </div>
                  <div className="form-group col-md-12 mb-3">
                    <label>Correo electrónico</label>
                    <input
                      type="email"
                      className="form-control"
                      name={'email'}
                      value={this.state.email}
                      onChange={this.handleChange}
                      required
                    />
                    {this.validator.message('email', this.state.email, 'required')}
                  </div>
                  <div className={'col-12 text-center'} style={{ marginBottom: '20px' }}>
                    <Link to={'/dashboard'}>Iniciar sesión</Link>
                  </div>
                  <div className={'col-12'}>
                    <input
                      className={"btn btn-primary btn-lg btn-block"}
                      value={'Recuperar mi contraseña'}
                      type={"submit"}
                    />
                  </div>
                </div>
              </form>
            </div>
            <div className={'col-6 Login__image'}>
              <img src={LoginImage} alt="Ingresar a Hachiko Pets" style={{borderRadius: '20px', width: '100%'}}/>
            </div>
          </div>
        </div>
      </BlueLayout>
    );
  }
}

export default ForgotPassword;
