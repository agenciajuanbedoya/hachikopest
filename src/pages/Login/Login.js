import React, {Component} from "react";
import {Link} from "react-router-dom";
import SimpleReactValidator from "simple-react-validator";
import {auth} from "../../core/firebase";
import LoginImage from '../../images/login.png';
import './styles/Login.css';
import BlueLayout from "../../layout/BlueLayout";
import {ToastsContainer, ToastsContainerPosition, ToastsStore} from "react-toasts";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      terms: false,
    };
    this.validator = new SimpleReactValidator({
      element: message => <div style={{color: 'red'}}>{message}</div>,
      messages: {
        default: 'Información requerida',
      }, autoForceUpdate: this});
  }
  handleChange = e => {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value,
    });
  };

  handleTerms = () => {
    if (this.state.terms === false) {
      this.setState({
        ...this.state,
        terms: true,
      });
    } else {
      this.setState({
        ...this.state,
        terms: false,
      });
    }
  };

  handleLogin = e => {
    e.preventDefault();
    if (this.validator.allValid()) {

      if (this.state.terms === true) {
        ToastsStore.info("Procesando información");
        auth.signInWithEmailAndPassword(this.state.email, this.state.password)
          .then(() => {
            ToastsStore.success("Bienvenido de vuelta");
          }).catch(err => {
          ToastsStore.error(err.message);
        });
      } else {
        ToastsStore.error('Acepte términos y condiciones para usar');
      }

    } else {
      this.validator.showMessages();
      ToastsStore.error("Información requerida");
      this.forceUpdate();
    }
  };

  render() {
    return (
      <BlueLayout>
        <div className={'container'} style={{ marginBottom: '20px' }}>
          <div className={'row justify-content-center align-items-center'} style={{marginTop: '40px'}}>
            <div className={'col-xl-4 col-lg-4 col-md-12 col-sm12 col-12 Login__main'}>

              <form noValidate onSubmit={this.handleLogin}>
                <div className="row">
                  <div className="col-12">
                    <h4 style={{ marginBottom: '30px', fontWeight: 'bold'}}>Ingresar</h4>
                  </div>
                  <div className="form-group col-md-12 mb-3">
                    <label>Correo electrónico</label>
                    <input
                      type="email"
                      className="form-control"
                      name={'email'}
                      value={this.state.email}
                      onChange={this.handleChange}
                      required
                    />
                    {this.validator.message('email', this.state.email, 'required')}
                  </div>

                  <div className="form-group col-md-12 mb-3">
                    <label>Contraseña</label>
                    <input
                      type="password"
                      className="form-control"
                      name={'password'}
                      value={this.state.password}
                      onChange={this.handleChange}
                      required
                    />
                    {this.validator.message('password', this.state.password, 'required')}
                  </div>

                  <div className="form-group form-check" style={{marginLeft: 16}}>
                    <input
                      type="checkbox"
                      className="form-check-input"
                      name={'terms'}
                      checked={this.state.terms}
                      onChange={this.handleTerms}
                      required
                    />
                    <label className="form-check-label">
                      Acepto los términos y condiciones de uso (<Link to={'/legal'}>Ver</Link>)
                    </label>
                  </div>

                  <div className={'col-12 text-center'} style={{ marginBottom: '20px' }}>
                    <Link to={'/forgot-password/'}>Recuperar mi contraseña</Link>
                  </div>
                  <div className={'col-12'}>
                    <input
                      className={"btn btn-primary btn-lg btn-block"}
                      value={'Iniciar sesión'}
                      type={"submit"}
                    />
                  </div>
                  <div className={'col-12 text-center'} style={{ marginBottom: 10, marginTop: 10 }}>
                    <Link className={'btn btn-warning btn-lg btn-block'} to={'/register/'}>Registro</Link>
                  </div>
                </div>
              </form>
              <ToastsContainer position={ToastsContainerPosition.BOTTOM_CENTER} store={ToastsStore}/>
            </div>
            <div className={'col-6 Login__image'}>
              <img src={LoginImage} alt="Ingresar a Hachiko Pets" style={{borderRadius: '20px', width: '100%'}}/>
            </div>
          </div>
        </div>
      </BlueLayout>
    );
  }
}

export default Login;
