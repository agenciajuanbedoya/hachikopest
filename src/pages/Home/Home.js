import React from 'react';
import Gallery from "../../components/Home/Gallery";
import LastProduct from "../../components/Home/LastProduct";
import ProductOrService from "../../components/Home/ProductOrService";
import ServicesDetails from "../../components/Home/ServicesDetails";
import Brands from "../../components/Home/Brands";
import Trending from "../../components/Home/Trending";
import AppDetails from "../../components/Home/AppDetails";
import BlueLayout from "../../layout/BlueLayout";

const Home = () => {
  return (
    <BlueLayout>
      <Gallery />
      <LastProduct />
      <ProductOrService />
      <ServicesDetails />
      <div className={"container"}>
        <Brands />
        <Trending />
        <AppDetails />
      </div>
    </BlueLayout>
  );
};

export default Home;
