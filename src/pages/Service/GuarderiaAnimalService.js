import React, { Component } from "react";
import {ToastsContainer, ToastsContainerPosition, ToastsStore} from "react-toasts";
import SimpleReactValidator from "simple-react-validator";
import Cookies from "js-cookie";
import BlueLayout from "../../layout/BlueLayout";
import './styles/index.css';

class GuarderiaAnimalService extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        type: "Perro",
        breed: "",
        age: "",
        service: "",
        days: "",
        phone: "",
        observations: "",
      },
    };
    this.validator = new SimpleReactValidator({
      element: message => <div style={{color: 'red'}}>{message}</div>,
      messages: {
        default: 'Información requerida',
      }, autoForceUpdate: this});
  };

  handleChange = e => {
    this.setState({
      ...this.state,
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });
  };

  handleSend = e => {
    e.preventDefault();
    if (this.validator.allValid()) {
      ToastsStore.info("Procesando información");
      const {type, breed, age, service, days, phone, observations} = this.state.form;
      const mailSubject = "Solicitud: Servicio de guardería";
      const mail = {
        Text: `${mailSubject}\nTipo: ${type}\nRaza: ${breed}\nEdad: ${age}\nServicio: ${service}\nDías: ${days}\nTeléfono: ${phone}\nObservaciones: ${observations}\nCiudad: ${Cookies.get('hachikopets_loc')}`,
        HTML: `<h1>${mailSubject}</h1>Tipo: ${type}<br />Raza: ${breed}<br />Edad: ${age}<br />Servicio: ${service}<br />Días: ${days}<br />Teléfono: ${phone}<br />Observaciones: ${observations}<br />Ciudad: ${Cookies.get('hachikopets_loc')}`,
      };

      const myHeaders = new Headers();
      myHeaders.append("Authorization", "Bearer U2FsdGVkX1+FgvJxKZc0mT5BeGDhG2joQvtxxhobgC0=");
      myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

      const urlencoded = new URLSearchParams();
      urlencoded.append("mailTo", "atencionalcliente@hachikopets.com");
      urlencoded.append("mailSubject", mailSubject);
      urlencoded.append("mailText", mail.Text);
      urlencoded.append("mailHTML", mail.HTML);

      const requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: urlencoded,
      };

      fetch("https://api.hachikopets.com/", requestOptions)
        .then(res => res.text())
        .then(() => {
          this.validator.hideMessages();
          this.setState({
            form: {
              type: "Perro",
              breed: "",
              age: "",
              service: "",
              days: "",
              phone: "",
              observations: "",
            },
          });
          ToastsStore.success("¡Envio éxitoso!");
        })
        .catch(err => {
          ToastsStore.error(err.message);
        });
    } else {
      this.validator.showMessages();
      ToastsStore.error("Información requerida");
    }
  };

  render() {
    return (
      <BlueLayout>
        <div className={'container'}>
          <div className="row Service__main">
            <div className="col-8">
              <form onSubmit={e => this.handleSend(e)} className="contact-form Service__contact-form">
                <div className="form-group">
                  <label className="control-label col-sm-12">Tipo de mascota</label>
                  <div className="col-sm-12">
                    <select
                      className="col-sm-12 Service__select"
                      name="type"
                      defaultValue={''}
                      value={this.state.form.type}
                      onChange={this.handleChange}
                    >
                      <option value={''} disabled>Elija una opción</option>
                      <option value={'Perro'}>Perro</option>
                      <option value={'Gato'}>Gato</option>
                    </select>
                    {this.validator.message('type', this.state.form.type, 'required')}
                  </div>
                </div>

                {this.state.form.type === "Perro" ? (
                  <div className="form-group">
                    <label className="control-label col-sm-12">¿Cual es la raza de su perro?</label>
                    <div className="col-sm-12">
                      <select
                        className="col-sm-12 Service__select"
                        name="breed"
                        defaultValue={''}
                        value={this.state.form.breed}
                        onChange={this.handleChange}
                      >
                        <option value={''} disabled>Elija la raza de su perro</option>
                        <option value={'Affenpinscher'}>Affenpinscher</option>
                        <option value={'Airedale terrier'}>Airedale terrier</option>
                        <option value={'Akita Americano'}>Akita Americano</option>
                        <option value={'Akita Inu'}>Akita Inu</option>
                        <option value={'Alano español'}>Alano español</option>
                        <option value={'Alaskan malamute'}>Alaskan malamute</option>
                        <option value={'American Hairless terrier'}>American Hairless terrier</option>
                        <option value={'American Staffordshire Terrier'}>American Staffordshire Terrier</option>
                        <option value={'Antiguo Perro Pastor Inglés'}>Antiguo Perro Pastor Inglés</option>
                        <option value={'Bóxer'}>Bóxer</option>
                        <option value={'Bardino (Perro majorero)'}>Bardino (Perro majorero)</option>
                        <option value={'Basenji'}>Basenji</option>
                        <option value={'Basset hound'}>Basset hound</option>
                        <option value={'Beagle'}>Beagle</option>
                        <option value={'Beauceron'}>Beauceron</option>
                        <option value={'Bichón maltés'}>Bichón maltés</option>
                        <option value={'Bichon frisé'}>Bichon frisé</option>
                        <option value={'Bloodhound'}>Bloodhound</option>
                        <option value={'Border collie'}>Border collie</option>
                        <option value={'Borzoi'}>Borzoi</option>
                        <option value={'Boston terrier'}>Boston terrier</option>
                        <option value={'Braco alemán de pelo corto'}>Braco alemán de pelo corto</option>
                        <option value={'Braco alemán de pelo duro'}>Braco alemán de pelo duro</option>
                        <option value={'Braco de Auvernia'}>Braco de Auvernia</option>
                        <option value={'Braco de Saint Germain'}>Braco de Saint Germain</option>
                        <option value={'Braco de Weimar'}>Braco de Weimar</option>
                        <option value={'Braco francés'}>Braco francés</option>
                        <option value={'Braco húngaro'}>Braco húngaro</option>
                        <option value={'Braco italiano'}>Braco italiano</option>
                        <option value={'Braco tirolés'}>Braco tirolés</option>
                        <option value={'Bull Terrier'}>Bull Terrier</option>
                        <option value={'Bulldog americano'}>Bulldog americano</option>
                        <option value={'Bulldog francés'}>Bulldog francés</option>
                        <option value={'Bulldog inglés'}>Bulldog inglés</option>
                        <option value={'Bullmastiff'}>Bullmastiff</option>
                        <option value={'Cão da Serra da Estrela'}>Cão da Serra da Estrela</option>
                        <option value={'Cão da Serra de Aires'}>Cão da Serra de Aires</option>
                        <option value={'Cão de Agua Português'}>Cão de Agua Português</option>
                        <option value={'Cão de Castro Laboreiro'}>Cão de Castro Laboreiro</option>
                        <option value={'Cão de Fila de São Miguel'}>Cão de Fila de São Miguel</option>
                        <option value={'Can de palleiro'}>Can de palleiro</option>
                        <option value={'Caniche'}>Caniche</option>
                        <option value={'Chihuahueño'}>Chihuahueño</option>
                        <option value={'Chow chow'}>Chow chow</option>
                        <option value={'Clumber spaniel'}>Clumber spaniel</option>
                        <option value={'Cocker spaniel americano'}>Cocker spaniel americano</option>
                        <option value={'Cocker spaniel inglés'}>Cocker spaniel inglés</option>
                        <option value={'Collie'}>Collie</option>
                        <option value={'Collie barbudo'}>Collie barbudo</option>
                        <option value={'Crestado Chino'}>Crestado Chino</option>
                        <option value={'Criollo'}>Criollo</option>
                        <option value={'Dálmata'}>Dálmata</option>
                        <option value={'Dachshund'}>Dachshund</option>
                        <option value={'Dobermann'}>Dobermann</option>
                        <option value={'Dogo argentino'}>Dogo argentino</option>
                        <option value={'Dogo de burdeos'}>Dogo de burdeos</option>
                        <option value={'Dogo guatemalteco'}>Dogo guatemalteco</option>
                        <option value={'Epagneul papillón'}>Epagneul papillón</option>
                        <option value={'Flat-Coated Retriever'}>Flat-Coated Retriever</option>
                        <option value={'Fox Terrier'}>Fox Terrier</option>
                        <option value={'Galgo español'}>Galgo español</option>
                        <option value={'Galgo húngaro'}>Galgo húngaro</option>
                        <option value={'Galgo inglés'}>Galgo inglés</option>
                        <option value={'Galgo italiano'}>Galgo italiano</option>
                        <option value={'Gegar colombiano'}>Gegar colombiano</option>
                        <option value={'Golden retriever'}>Golden retriever</option>
                        <option value={'Gran danés'}>Gran danés</option>
                        <option value={'Greyhound'}>Greyhound</option>
                        <option value={'Grifón belga'}>Grifón belga</option>
                        <option value={'Husky siberiano'}>Husky siberiano</option>
                        <option value={'Jack Russell Terrier'}>Jack Russell Terrier</option>
                        <option value={'Keeshond'}>Keeshond</option>
                        <option value={'Kerry blue terrier'}>Kerry blue terrier</option>
                        <option value={'Komondor'}>Komondor</option>
                        <option value={'Kuvasz'}>Kuvasz</option>
                        <option value={'Labrador'}>Labrador</option>
                        <option value={'Lebrel afgano'}>Lebrel afgano</option>
                        <option value={'Lhasa apso'}>Lhasa apso</option>
                        <option value={'Lobo de saarloos'}>Lobo de saarloos</option>
                        <option value={'Maltés'}>Maltés</option>
                        <option value={'Manchester terrier'}>Manchester terrier</option>
                        <option value={'Mastín afgano'}>Mastín afgano</option>
                        <option value={'Mastín del Pirineo'}>Mastín del Pirineo</option>
                        <option value={'Mastín español'}>Mastín español</option>
                        <option value={'Mastín inglés'}>Mastín inglés</option>
                        <option value={'Mastín napolitano'}>Mastín napolitano</option>
                        <option value={'Mastín tibetano'}>Mastín tibetano</option>
                        <option value={'Mucuchies'}>Mucuchies</option>
                        <option value={'Ovejero magallánico'}>Ovejero magallánico</option>
                        <option value={'Pastor alemán'}>Pastor alemán</option>
                        <option value={'Pastor belga'}>Pastor belga</option>
                        <option value={'Pastor blanco suizo'}>Pastor blanco suizo</option>
                        <option value={'Pastor catalán'}>Pastor catalán</option>
                        <option value={'Pastor croata'}>Pastor croata</option>
                        <option value={'Pastor de los Pirineos'}>Pastor de los Pirineos</option>
                        <option value={'Pastor garafiano'}>Pastor garafiano</option>
                        <option value={'Pastor holandés'}>Pastor holandés</option>
                        <option value={'Pastor leonés'}>Pastor leonés</option>
                        <option value={'Pastor mallorquín'}>Pastor mallorquín</option>
                        <option value={'Pastor peruano Chiribaya'}>Pastor peruano Chiribaya</option>
                        <option value={'Pastor vasco'}>Pastor vasco</option>
                        <option value={'Pekinés'}>Pekinés</option>
                        <option value={'Pembroke Welsh Corgi'}>Pembroke Welsh Corgi</option>
                        <option value={'Pequeño Lebrel Italiano'}>Pequeño Lebrel Italiano</option>
                        <option value={'Perdiguero francés'}>Perdiguero francés</option>
                        <option value={'Perdiguero portugués'}>Perdiguero portugués</option>
                        <option value={'Perro cimarrón uruguayo'}>Perro cimarrón uruguayo</option>
                        <option value={'Perro de agua americano'}>Perro de agua americano</option>
                        <option value={'Perro de agua español'}>Perro de agua español</option>
                        <option value={'Perro de agua irlandés'}>Perro de agua irlandés</option>
                        <option value={'Perro de agua portugués'}>Perro de agua portugués</option>
                        <option value={'Perro de Montaña de los Pirineos'}>Perro de Montaña de los Pirineos</option>
                        <option value={'Perro dogo mallorquín'}>Perro dogo mallorquín</option>
                        <option value={'Perro esquimal canadiense'}>Perro esquimal canadiense</option>
                        <option value={'Perro pastor de las islas Shetland'}>Perro pastor de las islas Shetland</option>
                        <option value={'Perro peruano sin pelo'}>Perro peruano sin pelo</option>
                        <option value={'Phalène'}>Phalène</option>
                        <option value={'Pinscher alemán'}>Pinscher alemán</option>
                        <option value={'Pinscher miniatura'}>Pinscher miniatura</option>
                        <option value={'Pitbull'}>Pitbull</option>
                        <option value={'Podenco canario'}>Podenco canario</option>
                        <option value={'Podenco ibicenco'}>Podenco ibicenco</option>
                        <option value={'Podenco portugués'}>Podenco portugués</option>
                        <option value={'Pointer'}>Pointer</option>
                        <option value={'Pomerania'}>Pomerania</option>
                        <option value={'Presa canario'}>Presa canario</option>
                        <option value={'Pug'}>Pug</option>
                        <option value={'Puli'}>Puli</option>
                        <option value={'Rafeiro do Alentejo'}>Rafeiro do Alentejo</option>
                        <option value={'Ratonero bodeguero andaluz'}>Ratonero bodeguero andaluz</option>
                        <option value={'Ratonero mallorquín'}>Ratonero mallorquín</option>
                        <option value={'Ratonero valenciano'}>Ratonero valenciano</option>
                        <option value={'Rhodesian Ridgeback'}>Rhodesian Ridgeback</option>
                        <option value={'Rottweiler'}>Rottweiler</option>
                        <option value={'Saluki'}>Saluki</option>
                        <option value={'Samoyedo'}>Samoyedo</option>
                        <option value={'San Bernardo'}>San Bernardo</option>
                        <option value={'Sato'}>Sato</option>
                        <option value={'Schnauzer estándar'}>Schnauzer estándar</option>
                        <option value={'Schnauzer gigante'}>Schnauzer gigante</option>
                        <option value={'Schnauzer miniatura'}>Schnauzer miniatura</option>
                        <option value={'Setter inglés'}>Setter inglés</option>
                        <option value={'Setter irlandés'}>Setter irlandés</option>
                        <option value={'Shar Pei'}>Shar Pei</option>
                        <option value={'Shiba Inu'}>Shiba Inu</option>
                        <option value={'Shih Tzu'}>Shih Tzu</option>
                        <option value={'Siberian husky'}>Siberian husky</option>
                        <option value={'Skye terrier'}>Skye terrier</option>
                        <option value={'Sussex spaniel'}>Sussex spaniel</option>
                        <option value={'Terranova'}>Terranova</option>
                        <option value={'Terrier alemán'}>Terrier alemán</option>
                        <option value={'Terrier australiano'}>Terrier australiano</option>
                        <option value={'Terrier brasileño'}>Terrier brasileño</option>
                        <option value={'Terrier chileno'}>Terrier chileno</option>
                        <option value={'Terrier escocés'}>Terrier escocés</option>
                        <option value={'Terrier galés'}>Terrier galés</option>
                        <option value={'Terrier irlandés'}>Terrier irlandés</option>
                        <option value={'Weimaraner'}>Weimaraner</option>
                        <option value={'West Highland White Terrier'}>West Highland White Terrier</option>
                        <option value={'Whippet'}>Whippet</option>
                        <option value={'Xoloitzcuintle'}>Xoloitzcuintle</option>
                        <option value={'Yorkshire terrier'}>Yorkshire terrier</option>
                        <option value={'Pastor Australiano'}>Pastor Australiano</option>
                        <option value={'Viejo Pastor Ovejero Inglés'}>Viejo Pastor Ovejero Inglés</option>
                        <option value={'Otra'}>Otra</option>
                      </select>
                      {this.validator.message('breed', this.state.form.breed, 'required')}
                    </div>
                  </div>
                ) : (
                  <div className="form-group">
                    <label className="control-label col-sm-12">¿Cual es la raza de su gato?</label>
                    <div className="col-sm-12">
                      <select
                        className="col-sm-12 Service__select"
                        name="breed"
                        defaultValue={''}
                        value={this.state.form.breed}
                        onChange={this.handleChange}
                      >
                        <option value={''} disabled>Elija la raza de su gato</option>
                        <option value={'Abisinio'}>Abisinio</option>
                        <option value={'American shorthair'}>American shorthair</option>
                        <option value={'Angora Turco'}>Angora Turco</option>
                        <option value={'Balinés'}>Balinés</option>
                        <option value={'Bengalí'}>Bengalí</option>
                        <option value={'Bobtail'}>Bobtail</option>
                        <option value={'Bombay'}>Bombay</option>
                        <option value={'Bosque de Noruega'}>Bosque de Noruega</option>
                        <option value={'British Shorthair'}>British Shorthair</option>
                        <option value={'Burmés'}>Burmés</option>
                        <option value={'Burmilla'}>Burmilla</option>
                        <option value={'Cornish Rex'}>Cornish Rex</option>
                        <option value={'Criollo'}>Criollo</option>
                        <option value={'Cymric'}>Cymric</option>
                        <option value={'Esfinge Gris'}>Esfinge Gris</option>
                        <option value={'Esfinge Piel'}>Esfinge Piel</option>
                        <option value={'Habana'}>Habana</option>
                        <option value={'Himalayo'}>Himalayo</option>
                        <option value={'Korat'}>Korat</option>
                        <option value={'Maine Coon'}>Maine Coon</option>
                        <option value={'Nubelung'}>Nubelung</option>
                        <option value={'Persa'}>Persa</option>
                        <option value={'Ragamuffin'}>Ragamuffin</option>
                        <option value={'Ragdoll'}>Ragdoll</option>
                        <option value={'Ruso Azul'}>Ruso Azul</option>
                        <option value={'Sagrado de Birmania'}>Sagrado de Birmania</option>
                        <option value={'Siamés'}>Siamés</option>
                        <option value={'Siberiano'}>Siberiano</option>
                        <option value={'Snowshoe'}>Snowshoe</option>
                        <option value={'U Hua Dragon Li'}>U Hua Dragon Li</option>
                        <option value={'Van Turco'}>Van Turco</option>
                        <option value={'Otra'}>Otra</option>
                      </select>
                      {this.validator.message('breed', this.state.form.breed, 'required')}
                    </div>
                  </div>
                )}

                <div className="form-group">
                  <label className="control-label col-sm-12">Edad de su mascota</label>
                  <div className="col-sm-12">
                    <select
                      className="col-sm-12 Service__select"
                      name="age"
                      defaultValue={''}
                      value={this.state.form.age}
                      onChange={this.handleChange}
                    >
                      <option value={''} disabled>Elija una opción</option>
                      <option value={'De 0 a 6 meses'}>De 0 a 6 meses</option>
                      <option value={'De 6 meses a 1 año'}>De 6 meses a 1 año</option>
                      <option value={'De 1 año a 3 años'}>De 1 año a 3 años</option>
                      <option value={'De 3 años a 5 años'}>De 3 años a 5 años</option>
                      <option value={'Mayor de 5 años'}>Mayor de 5 años</option>
                    </select>
                    {this.validator.message('age', this.state.form.age, 'required')}
                  </div>
                </div>

                <div className="form-group">
                  <label className="control-label col-sm-12">Servicio</label>
                  <div className="col-sm-12">
                    <select
                      className="col-sm-12 Service__select"
                      name="service"
                      defaultValue={''}
                      value={this.state.form.service}
                      onChange={this.handleChange}
                    >
                      <option value={''} disabled>Elija una opción</option>
                      <option value={'Adiestramiento'}>Adiestramiento</option>
                      <option value={'Guardería'}>Guardería</option>
                      <option value={'Colegio'}>Colegio</option>
                      <option value={'Hotel'}>Hotel</option>
                      <option value={'Otros'}>Otros</option>
                    </select>
                    {this.validator.message('service', this.state.form.service, 'required')}
                  </div>
                </div>

                <div className="form-group">
                  <label className="control-label col-sm-12">Días solicitados</label>
                  <div className="col-sm-12">
                    <input
                      className="form-control"
                      placeholder="1, 2, 3, 4..."
                      value={this.state.form.days}
                      onChange={this.handleChange}
                      type="number"
                      name="days"
                    />
                    {this.validator.message('days', this.state.form.days, 'required')}
                  </div>
                </div>

                <div className="form-group">
                  <label className="control-label col-sm-12">Número de teléfono</label>
                  <div className="col-sm-12">
                    <input
                      className="form-control"
                      placeholder="300 000 0000"
                      value={this.state.form.phone}
                      onChange={this.handleChange}
                      type="phone"
                      name="phone"
                    />
                    {this.validator.message('phone', this.state.form.phone, 'required')}
                  </div>
                </div>

                <div className="form-group">
                  <label className="control-label col-sm-12">Observaciones</label>
                  <div className="col-sm-12">
                    <textarea
                      className="form-control"
                      value={this.state.form.observations}
                      onChange={this.handleChange}
                      name="observations"
                      rows="5"
                    />
                  </div>
                </div>

                <ToastsContainer position={ToastsContainerPosition.BOTTOM_CENTER} store={ToastsStore}/>
                <div className="form-group">
                  <div className="col-sm-offset-2 col-sm-12">
                    <input type="submit" value="Enviar" className={"btn btn-primary btn-lg btn-block"} />
                  </div>
                </div>
              </form>
            </div>
            <div className="col-4">
              <div className="Service__sidebar-blue">
                <h2>Importante</h2>
                <p>Al diligenciar este formulario usted será contactado para confirmar su solicitud de servicio de guardería animal. Para esto debe estar atento al número de teléfono proporsionado.</p>
              </div>
            </div>
          </div>
        </div>
      </BlueLayout>
    );
  }
}

export default GuarderiaAnimalService;
