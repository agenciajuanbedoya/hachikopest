import React, { Component } from "react";
import BlueLayout from "../../layout/BlueLayout";
import './styles/index.css';

class Contact extends Component {
  render() {
    return (
      <BlueLayout>
        <div className={'container'}>
        <div className="row justify-content-center align-items-center">
          <div className="col-8">
            <div className="contact-form">
              <div className="form-group">
                <label className="control-label col-sm-2" htmlFor="fname">Nombres</label>
                <div className="col-sm-12">
                  <input type="text" className="form-control" id="fname" placeholder="Pedro" name="fname"/>
                </div>
              </div>
              <div className="form-group">
                <label className="control-label col-sm-2" htmlFor="lname">Apellidos</label>
                <div className="col-sm-12">
                  <input type="text" className="form-control" id="lname" placeholder="Peréz" name="lname"/>
                </div>
              </div>
              <div className="form-group">
                <label className="control-label col-sm-2" htmlFor="email">Email:</label>
                <div className="col-sm-12">
                  <input type="email" className="form-control" id="email" placeholder="tu@email.com" name="email"/>
                </div>
              </div>
              <div className="form-group">
                <label className="control-label col-sm-2" htmlFor="comment">Comentario</label>
                <div className="col-sm-12">
                  <textarea className="form-control" rows="5" id="comment"/>
                </div>
              </div>
              <div className="form-group">
                <div className="col-sm-offset-2 col-sm-10">
                  <button type="submit" className={"btn btn-default"}>Enviar</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </BlueLayout>
    );
  }
}

export default Contact;
