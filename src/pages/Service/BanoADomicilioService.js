import React, { Component } from "react";
import {ToastsContainer, ToastsContainerPosition, ToastsStore} from "react-toasts";
import SimpleReactValidator from "simple-react-validator";
import Cookies from "js-cookie";
import BlueLayout from "../../layout/BlueLayout";
import './styles/index.css';

class BanoADomicilioService extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        type: "Perro",
        weight: "",
        phone: "",
        date: "",
        stripe: "",
        observations: "",
      },
    };
    this.validator = new SimpleReactValidator({
      element: message => <div style={{color: 'red'}}>{message}</div>,
      messages: {
        default: 'Información requerida',
      }, autoForceUpdate: this});
  };

  handleChange = e => {
    this.setState({
      ...this.state,
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });
  };

  handleSend = e => {
    e.preventDefault();
    if (this.validator.allValid()) {
      ToastsStore.info("Procesando información");
      const {type, weight, phone, date, stripe, observations} = this.state.form;
      const mailSubject = "Solicitud: Servicio de baño a domicilio";
      const mail = {
        Text: `${mailSubject}\nTipo: ${type}\nTamaño: ${weight}\nTeléfono: ${phone}\nFecha: ${date}\nHorario: ${stripe}\nObservaciones: ${observations}\nCiudad: ${Cookies.get('hachikopets_loc')}`,
        HTML: `<h1>${mailSubject}</h1>Tipo: ${type}<br />Tamaño: ${weight}<br />Teléfono: ${phone}<br />Fecha: ${date}<br />Horario: ${stripe}<br />Observaciones: ${observations}<br />Ciudad: ${Cookies.get('hachikopets_loc')}`,
      };

      const myHeaders = new Headers();
      myHeaders.append("Authorization", "Bearer U2FsdGVkX1+FgvJxKZc0mT5BeGDhG2joQvtxxhobgC0=");
      myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

      const urlencoded = new URLSearchParams();
      urlencoded.append("mailTo", "atencionalcliente@hachikopets.com");
      urlencoded.append("mailSubject", mailSubject);
      urlencoded.append("mailText", mail.Text);
      urlencoded.append("mailHTML", mail.HTML);

      const requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: urlencoded,
      };

      fetch("https://api.hachikopets.com/", requestOptions)
        .then(res => res.text())
        .then(() => {
          this.validator.hideMessages();
          this.setState({
            form: {
              type: "Perro",
              weight: "",
              phone: "",
              date: "",
              stripe: "",
              observations: "",
            },
          });
          ToastsStore.success("¡Envio éxitoso!");
        })
        .catch(err => {
          ToastsStore.error(err.message);
        });
    } else {
      this.validator.showMessages();
      ToastsStore.error("Información requerida");
    }
  };

  render() {
    return (
      <BlueLayout>
        <div className={'container'}>
          <div className="row Service__main">
            <div className="col-8">
              <form onSubmit={e => this.handleSend(e)} className="contact-form Service__contact-form">
                <div className="form-group">
                  <label className="control-label col-sm-12">Tipo de mascota</label>
                  <div className="col-sm-12">
                    <select
                      className="col-sm-12 Service__select"
                      name="type"
                      defaultValue={''}
                      value={this.state.form.type}
                      onChange={this.handleChange}
                    >
                      <option value={''} disabled>Elija una opción</option>
                      <option value={'Perro'}>Perro</option>
                      <option value={'Gato'}>Gato</option>
                    </select>
                    {this.validator.message('type', this.state.form.type, 'required')}
                  </div>
                </div>

                <div className="form-group">
                  <label className="control-label col-sm-12">Tamaño</label>
                  <div className="col-sm-12">
                    <select
                      className="col-sm-12 Service__select"
                      name="weight"
                      defaultValue={''}
                      value={this.state.form.weight}
                      onChange={this.handleChange}
                    >
                      <option value={''} disabled>Elija una opción</option>
                      <option value={'Pequeño'}>Pequeño (25.000 $)</option>
                      <option value={'Mediano'}>Mediano (35.000 $)</option>
                      <option value={'Grande'}>Grande (50.000 $)</option>
                    </select>
                    {this.validator.message('weight', this.state.form.weight, 'required')}
                  </div>
                </div>

                <div className="form-group">
                  <label className="control-label col-sm-12">Número de teléfono</label>
                  <div className="col-sm-12">
                    <input
                      className="form-control"
                      placeholder="300 000 0000"
                      onChange={this.handleChange}
                      value={this.state.form.phone}
                      type="phone"
                      name="phone"
                    />
                    {this.validator.message('phone', this.state.form.phone, 'required')}
                  </div>
                </div>

                <div className="form-group">
                  <label className="control-label col-sm-12">Fecha</label>
                  <div className="col-sm-12">
                    <input
                      className="form-control"
                      placeholder="--/--/----"
                      onChange={this.handleChange}
                      value={this.state.form.date}
                      type="date"
                      name="date"
                    />
                    {this.validator.message('date', this.state.form.date, 'required')}
                  </div>
                </div>

                <div className="form-group">
                  <label className="control-label col-sm-12">Horario</label>
                  <div className="col-sm-12">
                    <select
                      className="col-sm-12 Service__select"
                      defaultValue={''}
                      onChange={this.handleChange}
                      value={this.state.form.stripe}
                      name="stripe"
                    >
                      <option value={''} disabled>Elija una opción</option>
                      <option value={'Pequeño'}>Mañana (8am - 11am)</option>
                      <option value={'Mediano'}>Tarde (12pm - 5pm)</option>
                      <option value={'Grande'}>Noche (5pm - 7pm)</option>
                    </select>
                    {this.validator.message('stripe', this.state.form.stripe, 'required')}
                  </div>
                </div>

                <div className="form-group">
                  <label className="control-label col-sm-12">Observaciones</label>
                  <div className="col-sm-12">
                    <textarea
                      className="form-control"
                      name="observations"
                      onChange={this.handleChange}
                      value={this.state.form.observations}
                      rows="5"
                    />
                  </div>
                </div>

                <ToastsContainer position={ToastsContainerPosition.BOTTOM_CENTER} store={ToastsStore}/>
                <div className="form-group">
                  <div className="col-sm-offset-2 col-sm-12">
                    <input type="submit" value="Enviar" className={"btn btn-primary btn-lg btn-block"} />
                  </div>
                </div>
              </form>
            </div>
            <div className="col-4">
              <div className="Service__sidebar-blue">
                <h2>Importante</h2>
                <p>Al diligenciar este formulario usted será contactado para confirmar su solicitud de servicio de guardería animal. Para esto debe estar atento al número de teléfono proporsionado.</p>
              </div>
            </div>
          </div>
        </div>
      </BlueLayout>
    );
  }
}

export default BanoADomicilioService;
