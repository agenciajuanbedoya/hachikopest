import React, { Component } from "react";
import {ToastsContainer, ToastsContainerPosition, ToastsStore} from "react-toasts";
import SimpleReactValidator from "simple-react-validator";
import Cookies from "js-cookie";
import BlueLayout from "../../layout/BlueLayout";
import './styles/index.css';

class DenunciaMaltratoService extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        type: "Perro",
        email: "",
        address: "",
        info: "",
      },
    };
    this.validator = new SimpleReactValidator({
      element: message => <div style={{color: 'red'}}>{message}</div>,
      messages: {
        default: 'Información requerida',
      }, autoForceUpdate: this});
  };

  handleChange = e => {
    this.setState({
      ...this.state,
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });
  };

  handleSend = e => {
    e.preventDefault();
    if (this.validator.allValid()) {
      ToastsStore.info("Procesando información");
      const {type, email, address, info} = this.state.form;
      const mailSubject = "Solicitud: Denuncia de maltrato";
      const mail = {
        Text: `${mailSubject}\nMascota: ${type}\nCorreo electrónico: ${email}\nDirección de hecho: ${address}\nRelato: ${info}\nCiudad: ${Cookies.get('hachikopets_loc')}`,
        HTML: `<h1>${mailSubject}</h1>Mascota: ${type}<br />Correo electrónico: ${email}<br />Dirección de hecho: ${address}<br />Relato: ${info}<br />Ciudad: ${Cookies.get('hachikopets_loc')}`,
      };

      const myHeaders = new Headers();
      myHeaders.append("Authorization", "Bearer U2FsdGVkX1+FgvJxKZc0mT5BeGDhG2joQvtxxhobgC0=");
      myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

      const urlencoded = new URLSearchParams();
      urlencoded.append("mailTo", "atencionalcliente@hachikopets.com");
      urlencoded.append("mailSubject", mailSubject);
      urlencoded.append("mailText", mail.Text);
      urlencoded.append("mailHTML", mail.HTML);

      const requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: urlencoded,
      };

      fetch("https://api.hachikopets.com/", requestOptions)
        .then(res => res.text())
        .then(() => {
          this.validator.hideMessages();
          this.setState({
            form: {
              type: "Perro",
              email: "",
              address: "",
              info: "",
            },
          });
          ToastsStore.success("¡Envio éxitoso!");
        })
        .catch(err => {
          ToastsStore.error(err.message);
        });
    } else {
      this.validator.showMessages();
      ToastsStore.error("Información requerida");
    }
  };

  render() {
    return (
      <BlueLayout>
        <div className={'container'}>
          <div className="row Service__main">
            <div className="col-8">
              <form onSubmit={e => this.handleSend(e)} className="contact-form Service__contact-form">
                <div className="form-group">
                  <label className="control-label col-sm-12">Tipo de mascota</label>
                  <div className="col-sm-12">
                    <select
                      className="col-sm-12 Service__select"
                      name="type"
                      defaultValue={''}
                      value={this.state.form.type}
                      onChange={this.handleChange}
                    >
                      <option value={''} disabled>Elija una opción</option>
                      <option value={'Perro'}>Perro</option>
                      <option value={'Gato'}>Gato</option>
                      <option value={'Otro'}>Otro</option>
                    </select>
                    {this.validator.message('type', this.state.form.type, 'required')}
                  </div>
                </div>

                <div className="form-group">
                  <label className="control-label col-sm-12">Correo electrónico</label>
                  <div className="col-sm-12">
                    <input
                      className="form-control"
                      placeholder="un@correo.com"
                      value={this.state.form.email}
                      onChange={this.handleChange}
                      type="email"
                      name="email"
                    />
                    {this.validator.message('email', this.state.form.email, 'required')}
                  </div>
                </div>

                <div className="form-group">
                  <label className="control-label col-sm-12">Dirección del hecho</label>
                  <div className="col-sm-12">
                    <input
                      className="form-control"
                      placeholder="Cra. 101 #23, Bogotá, Colombia"
                      value={this.state.form.address}
                      onChange={this.handleChange}
                      name="address"
                    />
                    {this.validator.message('address', this.state.form.address, 'required')}
                  </div>
                </div>

                <div className="form-group">
                  <label className="control-label col-sm-12">Relato</label>
                  <div className="col-sm-12">
                    <textarea
                      className="form-control"
                      value={this.state.form.info}
                      onChange={this.handleChange}
                      name="info"
                      rows="5"
                    />
                    {this.validator.message('info', this.state.form.info, 'required')}
                  </div>
                </div>

                <ToastsContainer position={ToastsContainerPosition.BOTTOM_CENTER} store={ToastsStore}/>
                <div className="form-group">
                  <div className="col-sm-offset-2 col-sm-12">
                    <input type="submit" value="Enviar" className={"btn btn-primary btn-lg btn-block"} />
                  </div>
                </div>
              </form>
            </div>
            <div className="col-4">
              <div className="Service__sidebar-blue">
                <h2>Importante</h2>
                <p>Al diligenciar este formulario usted será contactado para confirmar su solicitud de servicio de guardería animal. Para esto debe estar atento al número de teléfono proporsionado.</p>
              </div>
            </div>
          </div>
        </div>
      </BlueLayout>
    );
  }
}

export default DenunciaMaltratoService;
