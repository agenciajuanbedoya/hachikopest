import React, {Component} from "react";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import API from "../../core/Api";
import bgImage from "../../images/bg-hachikopets.jpg";
import BlueLayout from "../../layout/BlueLayout";
import CardProduct from "../../components/Cards/CardProduct";
import CategorySection from "../../components/Section/CategorySection";
import Cookies from "js-cookie";

class Category extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      docs: [],
      total: 0,
      limit: 0,
      page: 1,
      pages: 0,
    };
  }

  fetchProducts = async () => {
    const {attr} = this.props.match.params;
    if (this.state.pages > this.state.page) {
      this.setState({
        ...this.state,
        loading: true,
      });
      const res = await fetch(API + `/products/category?location=${Cookies.get('hachikopets_loc').toLowerCase()}&pet=${this.props.pet}&category=${attr}&page=${this.state.page}`);
      const data = await res.json();
      this.setState({
        ...this.state,
        docs: data.docs,
        total: data.total,
        limit: data.limit,
        page: data.page + 1,
        pages: data.pages,
        loading: false,
      });
    } else {
      console.log();
      this.setState({
        ...this.state,
        loading: true,
      });
      const res = await fetch(API + `/products/category?location=${Cookies.get('hachikopets_loc').toLowerCase()}&pet=${this.props.pet}&category=${attr}&page=${this.state.page}`);
      const data = await res.json();
      this.setState({
        ...this.state,
        docs: data.docs,
        total: data.total,
        limit: data.limit,
        page: data.page + 1,
        pages: data.pages,
        loading: false,
      });
    }
  };

  render() {
    return (
      <BlueLayout>
        <CategorySection
          attr={this.props.match.params.attr}
          colorText={"#ffffff"}
          select={this.props.match.params}
          pet={this.props.pet}
          background={bgImage}
          action={this.fetchData}
        />
        <section className={'container'}>
          <div className={'row'}>
            {this.state.loading ? (
              <div className={'loading'}>
                <div className="loader">Loading...</div>
              </div>
            ) : (
              <div className={'row'}>
                {this.state.docs.map((products, index) => (
                  <CardProduct data={products} key={index} />
                ))}
              </div>
            )}
          </div>
          <div className={'row'}>
            <div className="col-12 GridProducts__next">
              <Link to={'#'} onClick={this.fetchProducts}>
                Cargar más artículos
              </Link>
            </div>
          </div>
        </section>
      </BlueLayout>
    );
  }

  componentDidMount() {
    this.fetchProducts().then(r => {
      this.setState({
        ...this.state,
        loading: false
      });
    });
  }
}

const mapStateToProps = state => ({
  location: state.location,
  pet: state.pet
});

export default connect(mapStateToProps)(Category);
