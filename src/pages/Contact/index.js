import React, { Component } from "react";
import BlueLayout from "../../layout/BlueLayout";
import './styles/index.css';
import SimpleReactValidator from "simple-react-validator";
import {ToastsContainer, ToastsContainerPosition, ToastsStore} from "react-toasts";
import Cookies from "js-cookie";

class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        name: "",
        last_name: "",
        email: "",
        message: "",
      },
    };
    this.validator = new SimpleReactValidator({
      element: message => <div style={{color: 'red'}}>{message}</div>,
      messages: {
        default: 'Información requerida',
      }, autoForceUpdate: this});
  };

  handleChange = e => {
    this.setState({
      ...this.state,
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });
  };

  handleSend = e => {
    e.preventDefault();
    if (this.validator.allValid()) {
      ToastsStore.info("Procesando información");
      const {name, last_name, email, message} = this.state.form;
      const mailSubject = `Hachiko Pets: Nuevo mensaje de ${name}`;
      const mail = {
        Text: `${mailSubject}\nNombre: ${name}\nApellido: ${last_name}\nCorreo electrónico: ${email}\nMensaje: ${message}\nCiudad: ${Cookies.get('hachikopets_loc')}`,
        HTML: `<h1>${mailSubject}</h1>Nombre: ${name}<br />Apellido: ${last_name}<br />Correo electrónico: ${email}<br />Mensaje: ${message}<br />Ciudad: ${Cookies.get('hachikopets_loc')}`,
      };

      const myHeaders = new Headers();
      myHeaders.append("Authorization", "Bearer U2FsdGVkX1+FgvJxKZc0mT5BeGDhG2joQvtxxhobgC0=");
      myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

      const urlencoded = new URLSearchParams();
      urlencoded.append("mailTo", "atencionalcliente@hachikopets.com");
      urlencoded.append("mailSubject", mailSubject);
      urlencoded.append("mailText", mail.Text);
      urlencoded.append("mailHTML", mail.HTML);

      const requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: urlencoded,
      };

      fetch("https://api.hachikopets.com/", requestOptions)
        .then(res => res.text())
        .then(() => {
          this.validator.hideMessages();
          this.setState({
            form: {
              name: "",
              last_name: "",
              email: "",
              message: "",
            },
          });
          ToastsStore.success("¡Envio éxitoso!");
        })
        .catch(err => {
          ToastsStore.error(err.message);
        });
    } else {
      this.validator.showMessages();
      ToastsStore.error("Información requerida");
    }
  };

  render() {
    return (
      <BlueLayout>
        <div className={'container'}>
          <div className="row justify-content-center align-items-center">
            <div className="col-8">
              <form onSubmit={e => this.handleSend(e)} className="contact-form">
                <div className="form-group">
                  <label className="control-label col-sm-12">Nombres</label>
                  <div className="col-sm-12">
                    <input
                      className="form-control"
                      value={this.state.form.name}
                      onChange={this.handleChange}
                      type="text"
                      name="name"
                    />
                  </div>
                </div>

                <div className="form-group">
                  <label className="control-label col-sm-12">Apellidos</label>
                  <div className="col-sm-12">
                    <input
                      className="form-control"
                      value={this.state.form.last_name}
                      onChange={this.handleChange}
                      type="text"
                      name="last_name"
                    />
                  </div>
                </div>

                <div className="form-group">
                  <label className="control-label col-sm-12">Correo electrónico</label>
                  <div className="col-sm-12">
                    <input
                      className="form-control"
                      value={this.state.form.email}
                      onChange={this.handleChange}
                      type="email"
                      name="email"
                    />
                  </div>
                </div>

                <div className="form-group">
                  <label className="control-label col-sm-12">Mensaje</label>
                  <div className="col-sm-12">
                    <textarea
                      className="form-control"
                      value={this.state.form.message}
                      onChange={this.handleChange}
                      name="message"
                      rows="5"
                    />
                  </div>
                </div>

                <ToastsContainer position={ToastsContainerPosition.BOTTOM_CENTER} store={ToastsStore}/>
                <div className="form-group">
                  <div className="col-sm-offset-2 col-sm-12">
                    <input type="submit" value="Enviar" className={"btn btn-primary btn-lg btn-block"} />
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </BlueLayout>
    );
  }
}

export default Contact;
