import React, {Component} from "react";
import Logo from "../../images/notfound.png";
import bgImage from "../../images/bg-hachikopets.jpg";
import "./styles/index.css";

class NotFound extends Component {
  render() {
    return (
      <div className={'NotFound__container'} style={{backgroundImage: `url(${bgImage})`}}>
        <img className={'NotFound__image'} src={Logo} alt="Not Found 404"/>
        <div className={'NotFound__text'}>
          <h1>Error 404</h1>
          <span>Ubicación no disponible</span>
        </div>
      </div>
    );
  }
}

export default NotFound;
