import React from 'react';
import {
  faBan,
  faBone,
  faCamera,
  faChild,
  faCross,
  faHeart,
  faPaw,
  faStar,
  faTint
} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import BlueLayout from "../../layout/BlueLayout";
import CardService from "../../components/Cards/CardService";
import imgVeterinario from '../../images/services/veterinario.jpg';
import imgGuarderia from '../../images/services/guarderia.jpg';
import imgBano from '../../images/services/bano.jpg';
import imgFunerarios from '../../images/services/cementerio.jpg';
import imgEstudioFotografico from '../../images/services/fotografias.jpg';
import imgAdopcion from '../../images/services/adopcion.jpg';
import imgBuscadorParejas from '../../images/services/parejas.jpg';
import imgRestaurantes from '../../images/services/reastaurants.jpg';
import imgDenuncias from '../../images/services/denuncias.jpg';

const Services = ({location}) => {
  return (
    <BlueLayout>
      <div className={'container animated fadeInUp fast'} style={{ marginTop: '30px' }}>
        <div className={'row'}>

          <CardService
            title={'Veterinario en casa'}
            icon={<FontAwesomeIcon className={'CardService__container-icon'} icon={faPaw} />}
            background={imgVeterinario}
            link={'/servicio/veterinario-en-casa'}
          />

          <CardService
            title={'Guardería animal'}
            icon={<FontAwesomeIcon className={'CardService__container-icon'} icon={faChild} />}
            background={imgGuarderia}
            link={'/servicio/guarderia-animal'}
          />

          <CardService
            title={'Baño a domicilio'}
            icon={<FontAwesomeIcon className={'CardService__container-icon'} icon={faTint} />}
            background={imgBano}
            link={'/servicio/bano-a-domicilio'}
          />

          <CardService
            title={'Servicios funerarios'}
            icon={<FontAwesomeIcon className={'CardService__container-icon'} icon={faCross} />}
            background={imgFunerarios}
            link={'/servicio/servicios-funerarios'}
          />

          <CardService
            title={'Estudio fotográfico'}
            icon={<FontAwesomeIcon className={'CardService__container-icon'} icon={faCamera} />}
            background={imgEstudioFotografico}
            link={'/servicio/estudio-fotografico'}
          />

          <CardService
            title={'Adopción'}
            icon={<FontAwesomeIcon className={'CardService__container-icon'} icon={faStar} />}
            background={imgAdopcion}
            link={'/contact'}
          />

          <CardService
            title={'Buscador de pareja'}
            icon={<FontAwesomeIcon className={'CardService__container-icon'} icon={faHeart} />}
            background={imgBuscadorParejas}
            link={'/servicio/buscador-de-pareja'}
          />

          <CardService
            title={'Restaurantes Pet Friendly'}
            icon={<FontAwesomeIcon className={'CardService__container-icon'} icon={faBone} />}
            background={imgRestaurantes}
            link={'/contact'}
          />

          <CardService
            title={'Denunciar maltrato'}
            icon={<FontAwesomeIcon className={'CardService__container-icon'} icon={faBan} />}
            background={imgDenuncias}
            link={'/servicio/denunciar-maltrato'}
          />

        </div>
      </div>
    </BlueLayout>
  );
};

export default Services;
