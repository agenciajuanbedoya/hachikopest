import React from "react";
import PetModal from "../../components/Modal/PetModal";
import bgLogo from '../../images/bg-hachikopets.jpg';

import './styles/index.css';

const Welcome = () => {
  return(
    <div className={'WelcomePage__root'} style={{ backgroundImage: `url(${bgLogo})` }}>
      <PetModal />
    </div>
  )
};

export default Welcome;
