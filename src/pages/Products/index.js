import React, {Component} from "react";
import CategorySelector from "../../components/Selector/CategorySelector";
import GridProducts from "../../components/Grids/ProductsGrids";
import BlueLayout from "../../layout/BlueLayout";
import bgImage from "../../images/bg-hachikopets.jpg";

class Products extends Component {
  render() {
    return (
      <BlueLayout>
        <CategorySelector colorText={"#ffffff"} colorBg={"#004CC4"} background={bgImage} />
        <GridProducts />
      </BlueLayout>
    );
  }
}

export default Products;
