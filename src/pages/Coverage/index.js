import React from 'react';
import {connect} from 'react-redux';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faMapMarkerAlt} from "@fortawesome/free-solid-svg-icons";
import BlueLayout from "../../layout/BlueLayout";
import "./styles/index.css";

const Coverage = ({location}) => {
  if (location === 'Medellin') {
    return (
      <BlueLayout>
        <div className={'container-fluid'}>

          <div className={'row animated fadeInUp fast'} style={{ marginTop: '30px' }}>
            <div className="col-8">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d253821.6014370818!2d-75.73648093483924!3d6.268659411820874!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e4428ef4e52dddb%3A0x722fd6c39270ac72!2sMedell%C3%ADn%2C%20Antioquia!5e0!3m2!1ses-419!2sco!4v1583382422618!5m2!1ses-419!2sco"
                width={"100%"}
                height={"450"}
                frameBorder={"0"}
                style={{ border: 0 }}
                allowFullScreen
                title={"Bogotá"}
              />
            </div>
            <div className="col-4">
              <h2 className={'Coverage__title'}>Domicilios en <strong>Medellín</strong></h2>
              <h3 className={'Coverage__subtitle'}>Puntos de referencia para los domicilios</h3>
              <ul className={'Coverage__list'}>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Bello</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Itaguí</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Medellín</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Sabaneta</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Envigado</li>
              </ul>
            </div>
          </div>

        </div>
      </BlueLayout>
    );
  } else if (location === 'Bogota') {
    return (
      <BlueLayout>
        <div className={'container-fluid'}>

          <div className={'row animated fadeInUp fast'} style={{ marginTop: '30px' }}>
            <div className="col-4">
              <h2 className={'Coverage__title'}>Domicilios en <strong>Bogotá</strong></h2>
              <h3 className={'Coverage__subtitle'}>Puntos de referencia para los domicilios</h3>
              <ul className={'Coverage__list'}>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Carrera 7, Con Calle 170</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Avenida Circunvalar, Con Calle 7</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Carrera 10, Con Calle 7</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Avenida Primera de Mayo, Con Carrera 10</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Av. Ciudad de Cali & Av. 1 de Mayo</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Carrera 86, Con Calle 90</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Avenida Ciudad de Cali, Con Calle 127</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Avenida Ciudad de Cali, Con Calle 160</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Avenida Conejera, Con Calle 170</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Calle 183, Con Carrera 78</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Carrera 7, Con Calle 200</li>
              </ul>
            </div>
            <div className="col-8">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d254508.5164107565!2d-74.2478938043006!3d4.648283717342738!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e3f9bfd2da6cb29%3A0x239d635520a33914!2zQm9nb3TDoQ!5e0!3m2!1ses-419!2sco!4v1583379113782!5m2!1ses-419!2sco"
                width={"100%"}
                height={"450"}
                frameBorder={"0"}
                style={{ border: 0 }}
                allowFullScreen
                title={"Bogotá"}
              />
            </div>
          </div>

        </div>
      </BlueLayout>
    );
  } else if (location === 'Bucaramanga') {
    return (
      <BlueLayout>
        <div className={'container-fluid'}>

          <div className={'row animated fadeInUp fast'} style={{ marginTop: '30px' }}>
            <div className="col-4">
              <h2 className={'Coverage__title'}>Domicilios en <strong>Bucaramanga</strong></h2>
              <h3 className={'Coverage__subtitle'}>Puntos de referencia para los domicilios</h3>
              <ul className={'Coverage__list'}>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Bucaramanga</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Floridablanca</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Piedecuestas</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Girón</li>
              </ul>
            </div>
            <div className="col-8">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63344.93804202622!2d-73.1679977412265!3d7.119204683857606!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e68157af751c0ed%3A0x75a0e4551148c36c!2sBucaramanga%2C%20Santander!5e0!3m2!1ses-419!2sco!4v1583382101236!5m2!1ses-419!2sco"
                width={"100%"}
                height={"450"}
                frameBorder={"0"}
                style={{ border: 0 }}
                allowFullScreen
                title={"Bogotá"}
              />
            </div>
          </div>

        </div>
      </BlueLayout>
    );
  } else {
    return (
      <BlueLayout>
        <div className={'container-fluid'}>

          <div className={'row animated fadeInUp fast'} style={{ marginTop: '30px' }}>
            <div className="col-8">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d254508.5164107565!2d-74.2478938043006!3d4.648283717342738!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e3f9bfd2da6cb29%3A0x239d635520a33914!2zQm9nb3TDoQ!5e0!3m2!1ses-419!2sco!4v1583379113782!5m2!1ses-419!2sco"
                width={"100%"}
                height={"450"}
                frameBorder={"0"}
                style={{ border: 0 }}
                allowFullScreen
                title={"Bogotá"}
              />
            </div>
            <div className="col-4">
              <h2 className={'Coverage__title'}>Domicilios en <strong>Bogotá</strong></h2>
              <h3 className={'Coverage__subtitle'}>Puntos de referencia para los domicilios</h3>
              <ul className={'Coverage__list'}>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Carrera 7, Con Calle 170</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Avenida Circunvalar, Con Calle 7</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Carrera 10, Con Calle 7</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Avenida Primera de Mayo, Con Carrera 10</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Av. Ciudad de Cali & Av. 1 de Mayo</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Carrera 86, Con Calle 90</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Avenida Ciudad de Cali, Con Calle 127</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Avenida Ciudad de Cali, Con Calle 160</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Avenida Conejera, Con Calle 170</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Calle 183, Con Carrera 78</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Carrera 7, Con Calle 200</li>
              </ul>
            </div>
          </div>

          <div className={'row animated fadeInUp fast'} style={{ marginTop: '30px' }}>
            <div className="col-4">
              <h2 className={'Coverage__title'}>Domicilios en <strong>Bucaramanga</strong></h2>
              <h3 className={'Coverage__subtitle'}>Puntos de referencia para los domicilios</h3>
              <ul className={'Coverage__list'}>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Bucaramanga</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Floridablanca</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Piedecuestas</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Girón</li>
              </ul>
            </div>
            <div className="col-8">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63344.93804202622!2d-73.1679977412265!3d7.119204683857606!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e68157af751c0ed%3A0x75a0e4551148c36c!2sBucaramanga%2C%20Santander!5e0!3m2!1ses-419!2sco!4v1583382101236!5m2!1ses-419!2sco"
                width={"100%"}
                height={"450"}
                frameBorder={"0"}
                style={{ border: 0 }}
                allowFullScreen
                title={"Bogotá"}
              />
            </div>
          </div>

          <div className={'row animated fadeInUp fast'} style={{ marginTop: '30px' }}>
            <div className="col-8">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d253821.6014370818!2d-75.73648093483924!3d6.268659411820874!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e4428ef4e52dddb%3A0x722fd6c39270ac72!2sMedell%C3%ADn%2C%20Antioquia!5e0!3m2!1ses-419!2sco!4v1583382422618!5m2!1ses-419!2sco"
                width={"100%"}
                height={"450"}
                frameBorder={"0"}
                style={{ border: 0 }}
                allowFullScreen
                title={"Bogotá"}
              />
            </div>
            <div className="col-4">
              <h2 className={'Coverage__title'}>Domicilios en <strong>Medellín</strong></h2>
              <h3 className={'Coverage__subtitle'}>Puntos de referencia para los domicilios</h3>
              <ul className={'Coverage__list'}>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Bello</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Itaguí</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Medellín</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Sabaneta</li>
                <li className={'Coverage__list-item'}><FontAwesomeIcon className={'Coverage__list-icon'} icon={faMapMarkerAlt} /> Envigado</li>
              </ul>
            </div>
          </div>

        </div>
      </BlueLayout>
    );
  }
};

const mapStateToProps = state => ({
  location: state.location
});

export default connect(mapStateToProps)(Coverage);
