import React, {Component} from "react";
import SimpleReactValidator from "simple-react-validator";
import {connect} from "react-redux";
import BlueLayout from "../../layout/BlueLayout";
import DashboardSideBar from "../../components/SideBar/DashboardSideBar";
import {database, auth} from "../../core/firebase";
import './styles/Dashboard.css';

class PetsDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      memberships: [],
    };
    this.validator = new SimpleReactValidator({
      element: message => <div style={{color: 'red'}}>{message}</div>,
      messages: {
        default: 'Información requerida',
      }, autoForceUpdate: this});
  }

  fetchPets = () => {
    database.ref('memberships/' + auth.currentUser.uid).on('child_added', snap => {
      this.setState({
        ...this.state,
        memberships: [
          ...this.state.memberships,
          snap.val(),
        ],
      });
    });
  };

  render() {
    return (
      <BlueLayout>
        <div className="Dashboard__main">
          <div className={'container'}>
            <div className={'row'}>
              <div className="col-3">
                <DashboardSideBar />
              </div>
              <div className="col-9 Dashboard__page">
                <div className="row">
                  <div className="col-12">
                    <h3 className={"mb-3"}>Mis membresías</h3>
                  </div>
                </div>
                <div className="row">

                  {this.state.memberships.map(item => (
                    <div className="col-4">
                      <div className="card text-white bg-primary mb-3" style={{marginTop: 10}}>
                        <div className="card-body">
                          <h5 className="card-title">Hachiko Pets VIP <span
                            className="badge badge-light">PROCESANDO</span></h5>
                          <p className="card-text">Su memebresía realizado está siendo procesado.</p>
                          {/*<Link to={'#'} className="btn btn-warning" onClick={() => console.log('Hi')}>Eliminar</Link>*/}
                        </div>
                      </div>
                    </div>
                  ))}

                </div>
              </div>
            </div>
          </div>
        </div>
      </BlueLayout>
    );
  }

  componentDidMount() {
    this.fetchPets();
  }

  componentWillUnmount() {
    this.fetchPets();
  }
}

const mapStateToProps = state => ({
  uid: state.user.uid,
  user: state.user,
});

export default connect(mapStateToProps)(PetsDashboard);
