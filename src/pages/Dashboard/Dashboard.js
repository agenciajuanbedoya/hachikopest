import React, {Component} from "react";
import {ToastsContainer, ToastsContainerPosition, ToastsStore} from "react-toasts";
import SimpleReactValidator from "simple-react-validator";
import {connect} from "react-redux";
import BlueLayout from "../../layout/BlueLayout";
import DashboardSideBar from "../../components/SideBar/DashboardSideBar";
import {database, auth} from "../../core/firebase";
import './styles/Dashboard.css';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      last_name: '',
      doc_type: 'CC',
      doc_number: '',
      phone: '',
      email: '',
      instagram: '',
      facebook: '',
      country: 'Colombia',
      city: '',
      address: '',
      address2: '',
    };
    this.validator = new SimpleReactValidator({
      element: message => <div style={{color: 'red'}}>{message}</div>,
      messages: {
        default: 'Información requerida',
      }, autoForceUpdate: this});
  }

  handleCreateUser = e => {
    e.preventDefault();
    if (this.validator.allValid()) {

      database.ref('users/' + auth.currentUser.uid)
        .set(this.state)
        .then(r => {
          auth.currentUser.updateEmail(this.state.email)
            .then(() => {
              ToastsStore.success("¡Actualización exitosa!");
            })
            .catch(err => {
              ToastsStore.error(err.message);
            });
        })
        .catch(err => {
          ToastsStore.error(err.message);
        });

    } else {
      this.validator.showMessages();
      ToastsStore.error("Error al registrarse");
      this.forceUpdate();
    }
  };

  fetchUser = () => {
    if (auth.currentUser) {
      if (auth.currentUser.uid !== null) {
        database.ref('users/' + auth.currentUser.uid).on('value', snap => {
          this.setState(snap.val());
        });
      }
    }
  };

  handleChange = e => {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value,
    });
  };

  render() {
    return (
      <BlueLayout>
        <div className="Dashboard__main">
          <div className={'container'}>
            <div className={'row'}>
              <div className="col-3">
                <DashboardSideBar />
              </div>
              <div className="col-9 Dashboard__page">
                <div className="row">
                  <div className="col-12">
                    <h3 className={"mb-3"}>Mi cuenta</h3>
                  </div>
                </div>
                <form className="row" noValidate onSubmit={this.handleCreateUser}>

                  <div className="form-group col-md-6 mb-3">
                    <label>Nombre</label>
                    <input
                      type="text"
                      className="form-control"
                      name="name"
                      value={this.state.name}
                      onChange={this.handleChange}
                      required
                    />
                    {this.validator.message('name', this.state.name, 'required')}
                  </div>

                  <div className="form-group col-md-6 mb-3">
                    <label>Apellido</label>
                    <input
                      type="text"
                      className="form-control"
                      name="last_name"
                      value={this.state.last_name}
                      onChange={this.handleChange}
                      required
                    />
                    {this.validator.message('last_name', this.state.last_name, 'required')}
                  </div>

                  <div className="form-group col-md-6 mb-3">
                    <label>Tipo de documento</label>
                    <select
                      className="custom-select d-block w-100"
                      name="doc_type"
                      defaultValue={'CC'}
                      value={this.state.doc_type}
                      onChange={this.handleChange}
                    >
                      <option value={'CC'}>Cédula de ciudadanía</option>
                      <option value={'CE'}>Cédula de extrangería</option>
                      <option value={'DNI'}>Identificación extranjera</option>
                      <option value={'PPN'}>Pasaporte</option>
                      <option value={'NIT'}>NIT</option>
                    </select>
                  </div>

                  <div className="form-group col-md-6 mb-3">
                    <label>Número de cédula o de identificación</label>
                    <input
                      type="number"
                      min={0}
                      className="form-control"
                      name="doc_number"
                      value={this.state.doc_number}
                      onChange={this.handleChange}
                      required
                    />
                    {this.validator.message('doc_number', this.state.doc_number, 'required')}
                  </div>

                  <div className="form-group col-md-4 mb-3">
                    <label>Número de teléfono</label>
                    <input
                      type="phone"
                      min={0}
                      className="form-control"
                      name="phone"
                      value={this.state.phone}
                      onChange={this.handleChange}
                      required
                    />
                    {this.validator.message('phone', this.state.phone, 'required')}
                  </div>

                  <div className="form-group col-md-4 mb-3">
                    <label>Correo electrónico</label>
                    <input
                      type="email"
                      className="form-control"
                      name="email"
                      value={this.state.email}
                      onChange={this.handleChange}
                      required
                    />
                    {this.validator.message('email', this.state.email, 'required')}
                  </div>

                  <div className="form-group col-md-4 mb-3">
                    <label>País</label>
                    <input
                      type="text"
                      disabled
                      className="form-control"
                      name={'country'}
                      value={this.state.country}
                      onChange={this.handleChange}
                      required
                    />
                  </div>
                  <div className="form-group col-md-4 mb-3">
                    <label>Ciudad</label>
                    <input
                      type="text"
                      className="form-control"
                      name={'city'}
                      value={this.state.city}
                      onChange={this.handleChange}
                      required
                    />
                    {this.validator.message('city', this.state.city, 'required')}
                  </div>
                  <div className="form-group col-md-4 mb-3">
                    <label>Dirección</label>
                    <input
                      type="text"
                      className="form-control"
                      name={'address'}
                      value={this.state.address}
                      onChange={this.handleChange}
                      required
                    />
                    {this.validator.message('address', this.state.address, 'required')}
                  </div>
                  <div className="form-group col-md-4 mb-3">
                    <label>Referencia de dirección</label>
                    <input
                      type="text"
                      className="form-control"
                      name={'address2'}
                      value={this.state.address2}
                      onChange={this.handleChange}
                      required
                    />
                    {this.validator.message('address2', this.state.address2, 'required')}
                  </div>

                  <ToastsContainer position={ToastsContainerPosition.BOTTOM_CENTER} store={ToastsStore}/>
                  <div className="col-md-12">
                    <input
                      className={"btn btn-primary btn-lg btn-block"}
                      value={'Actualizar mis datos'}
                      type={"submit"}
                    />
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </BlueLayout>
    );
  }

  componentDidMount() {
    this.fetchUser();
  }

  componentWillUnmount() {
    this.fetchUser();
  }
}

const mapStateToProps = state => ({
  // uid: state.user.uid,
  user: state.user,
});

export default connect(mapStateToProps)(Dashboard);
