import React, {Component} from "react";
import SimpleReactValidator from "simple-react-validator";
import {connect} from "react-redux";
import BlueLayout from "../../layout/BlueLayout";
import DashboardSideBar from "../../components/SideBar/DashboardSideBar";
import {database, auth} from "../../core/firebase";
import './styles/Dashboard.css';

class PetsDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orders: [],
    };
    this.validator = new SimpleReactValidator({
      element: message => <div style={{color: 'red'}}>{message}</div>,
      messages: {
        default: 'Información requerida',
      }, autoForceUpdate: this});
  }

  fetchPets = () => {
    database.ref('orders/' + auth.currentUser.uid).on('child_added', snap => {
      this.setState({
        ...this.state,
        orders: [
          ...this.state.orders,
          snap.val(),
        ],
      });
    });
  };

  render() {
    return (
      <BlueLayout>
        <div className="Dashboard__main">
          <div className={'container'}>
            <div className={'row'}>
              <div className="col-3">
                <DashboardSideBar />
              </div>
              <div className="col-9 Dashboard__page">
                <div className="row">
                  <div className="col-12">
                    <h3 className={"mb-3"}>Mis pedidos</h3>
                  </div>
                </div>
                <div className="row">

                  {this.state.orders.map(item => (
                    <div className="col-4">
                      <div className="card text-white bg-primary mb-3" style={{marginTop: 10}}>
                        <div className="card-body">
                          <h5 className="card-title">Pedido <span
                            className="badge badge-light">PROCESANDO</span></h5>
                          <p className="card-text">Su pedidos realizado está siendo procesado.</p>
                          {/*<Link to={'#'} className="btn btn-warning" onClick={() => console.log('Hi')}>Eliminar</Link>*/}
                        </div>
                      </div>
                    </div>
                  ))}

                </div>
              </div>
            </div>
          </div>
        </div>
      </BlueLayout>
    );
  }

  componentDidMount() {
    this.fetchPets();
  }

  componentWillUnmount() {
    this.fetchPets();
  }
}

const mapStateToProps = state => ({
  uid: state.user.uid,
  user: state.user,
});

export default connect(mapStateToProps)(PetsDashboard);
