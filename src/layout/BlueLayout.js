import React from 'react';
import bgImage from "../images/bg-hachikopets.jpg";
import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";

class BlueLayout extends React.Component {
  render() {
    return (
      <div className={'Home__container'} style={{ backgroundImage: `url(${bgImage})` }}>
        <Header colorText={"#ffffff"} colorBg={"#004CC4"} />
        {this.props.children}
        <Footer color={"#004CC4"} />
      </div>
    );
  }
}

export default BlueLayout;
