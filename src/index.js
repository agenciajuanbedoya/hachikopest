import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import App from "./core/App";
// import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootswatch/dist/flatly/bootstrap.min.css';
import 'animate.css/animate.min.css';
import './index.css';

ReactDOM.render(<App />, document.getElementById('main'));

serviceWorker.unregister();
